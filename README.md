# Praun Industries

Discord: https://discord.gg/PyE5REF4ZM

# Tools used
- Blockbench for modelling blocks and items
- GIMP for texture creation

# Licensing
All textures if not stated otherwise, are under ARR, as most of them are texture mashes of vanilla textures.