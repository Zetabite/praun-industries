package zetabite.praunindustries.block;

// Minecraft / Mojang
import net.minecraft.block.*;
import net.minecraft.client.render.RenderLayer;

// Fabric
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.rendering.v1.ColorProviderRegistry;

// Quilt
import net.minecraft.sound.BlockSoundGroup;
import org.quiltmc.qsl.block.extensions.api.QuiltBlockSettings;
import org.quiltmc.qsl.block.extensions.api.client.BlockRenderLayerMap;

// Praun Industries
import zetabite.praunindustries.util.ModContentID;
import zetabite.praunindustries.bees.BeeType;
import zetabite.praunindustries.bees.block.BeeNestBlock;
import zetabite.praunindustries.bees.block.entity.BeeNestBlockEntity;
import zetabite.praunindustries.machines.block.*;
import zetabite.praunindustries.util.ContentRegistryUtil;

public class ModBlocks extends ContentRegistryUtil {

    public static final Block COKE_OVEN_PERIPHERAL;
    public static final Block COKE_OVEN_BRICK;
    public static final Block COKE_OVEN_CONTROLLER;
    public static final Block BEE_NEST;

    static {
        QuiltBlockSettings cokeOvenBlockSettings = QuiltBlockSettings.copyOf(AbstractBlock.Settings.of(Material.STONE, MapColor.BROWN).requiresTool().strength(2.0F, 6.0F).sounds(BlockSoundGroup.DEEPSLATE));
        COKE_OVEN_CONTROLLER = registerBlock(ModContentID.COKE_OVEN_CONTROLLER, new CokeOvenController(cokeOvenBlockSettings));
        COKE_OVEN_PERIPHERAL = registerBlock(ModContentID.COKE_OVEN_PERIPHERAL, new CokeOvenPeripheral(cokeOvenBlockSettings));
        COKE_OVEN_BRICK = registerBlock(ModContentID.COKE_OVEN_BRICK, new CokeOvenBrick(QuiltBlockSettings.copyOf(cokeOvenBlockSettings)));
        BEE_NEST = registerBlock(ModContentID.BEE_NEST, new BeeNestBlock(QuiltBlockSettings.copyOf(Blocks.STONE).requiresTool().strength(3.5F).nonOpaque()));

    }

    public static void onInitialize() {
    }

    @Environment(EnvType.CLIENT)
    public static void onInitializeClient() {
        BlockRenderLayerMap.put(RenderLayer.getCutout(), BEE_NEST);
        ColorProviderRegistry.BLOCK.register((state, view, pos, tintIndex) -> {
            int color = -1;
            if (view != null && view.getBlockEntity(pos) instanceof BeeNestBlockEntity nest) {
                color = BeeType.getColor(nest.getBeeType(), tintIndex);
            }
            return color;
        }, BEE_NEST);
    }
}
