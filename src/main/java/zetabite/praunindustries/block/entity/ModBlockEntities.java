package zetabite.praunindustries.block.entity;

// Minecraft / Mojang
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

// Fabric
import net.fabricmc.fabric.api.object.builder.v1.block.entity.FabricBlockEntityTypeBuilder;

// Praun Industries
import zetabite.praunindustries.util.ModContentID;
import zetabite.praunindustries.bees.block.entity.BeeNestBlockEntity;
import zetabite.praunindustries.block.ModBlocks;
import zetabite.praunindustries.machines.block.entity.CokeOvenControllerEntity;
import zetabite.praunindustries.machines.block.entity.CokeOvenPeripheralEntity;
import zetabite.praunindustries.util.ContentRegistryUtil;

import static zetabite.praunindustries.PraunIndustries.MODID;


public class ModBlockEntities extends ContentRegistryUtil {

    public static final BlockEntityType<CokeOvenControllerEntity> COKE_OVEN_CONTROLLER_ENTITY;
    public static final BlockEntityType<CokeOvenPeripheralEntity> COKE_OVEN_PERIPHERAL_ENTITY;
    public static final BlockEntityType<BeeNestBlockEntity> BEE_NEST;

    static {
        COKE_OVEN_CONTROLLER_ENTITY = Registry.register(Registry.BLOCK_ENTITY_TYPE, new Identifier(MODID, ModContentID.COKE_OVEN_CONTROLLER), FabricBlockEntityTypeBuilder.create(CokeOvenControllerEntity::new, ModBlocks.COKE_OVEN_CONTROLLER).build(null));
        COKE_OVEN_PERIPHERAL_ENTITY = Registry.register(Registry.BLOCK_ENTITY_TYPE, new Identifier(MODID, ModContentID.COKE_OVEN_PERIPHERAL), FabricBlockEntityTypeBuilder.create(CokeOvenPeripheralEntity::new, ModBlocks.COKE_OVEN_PERIPHERAL).build(null));
        BEE_NEST = Registry.register(Registry.BLOCK_ENTITY_TYPE, new Identifier(MODID, ModContentID.BEE_NEST), FabricBlockEntityTypeBuilder.create(BeeNestBlockEntity::new, ModBlocks.BEE_NEST).build(null));
    }

    public static void onInitialize() {
    }
}
