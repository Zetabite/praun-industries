package zetabite.praunindustries;

// Fabric
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;

// Quilt
import org.quiltmc.loader.api.ModContainer;
import org.quiltmc.qsl.base.api.entrypoint.client.ClientModInitializer;

// Praun Industries
import zetabite.praunindustries.block.ModBlocks;
import zetabite.praunindustries.entity.ModEntities;
import zetabite.praunindustries.fluid.ModFluids;
import zetabite.praunindustries.item.ModItems;
import zetabite.praunindustries.screen.ModScreenHandlers;

@Environment(EnvType.CLIENT)
public class PraunIndustriesClient implements ClientModInitializer {
    @Override
    public void onInitializeClient(ModContainer mod) {
        ModBlocks.onInitializeClient();
        ModScreenHandlers.onInitializeClient();
        ModItems.onInitializeClient();
        ModFluids.onInitializeClient();
        ModEntities.onInitializeClient();
    }
}
