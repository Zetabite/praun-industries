package zetabite.praunindustries.item;

// Minecraft / Mojang
import net.fabricmc.fabric.api.registry.FuelRegistry;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.nbt.NbtCompound;

// Fabric
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.rendering.v1.ColorProviderRegistry;

// Praun Industries
import zetabite.praunindustries.util.ModContentID;
import zetabite.praunindustries.bees.BeeType;
import zetabite.praunindustries.block.ModBlocks;
import zetabite.praunindustries.util.ContentRegistryUtil;

import static zetabite.praunindustries.PraunIndustries.ITEM_GROUP;


public class ModItems extends ContentRegistryUtil {

    public static final BlockItem COKE_OVEN_BRICK;
    public static final BlockItem COKE_OVEN_PERIPHERAL;
    public static final BlockItem COKE_OVEN_CONTROLLER;
    public static final BlockItem BEE_NEST;
    public static final Item COAL_COKE;

    static {
        COKE_OVEN_BRICK = registerBlockItem(ModContentID.COKE_OVEN_BRICK, ModBlocks.COKE_OVEN_BRICK, ITEM_GROUP);
        COKE_OVEN_PERIPHERAL = registerBlockItem(ModContentID.COKE_OVEN_PERIPHERAL, ModBlocks.COKE_OVEN_PERIPHERAL);
        COKE_OVEN_CONTROLLER = registerBlockItem(ModContentID.COKE_OVEN_CONTROLLER, ModBlocks.COKE_OVEN_CONTROLLER);
        COAL_COKE = registerItem(ModContentID.COAL_COKE, ITEM_GROUP);
        BEE_NEST = registerBlockItem(ModContentID.BEE_NEST, ModBlocks.BEE_NEST);
    }

    @Environment(EnvType.CLIENT)
    public static void onInitializeClient() {
        ColorProviderRegistry.ITEM.register((stack, tintIndex) -> {
            NbtCompound nbt = stack.getNbt();
            int color = -1;
            if (nbt != null && nbt.contains("BlockEntityTag")) {
                NbtCompound beTag = nbt.getCompound("BlockEntityTag");
                if (beTag.contains("beeType")) {
                    color = BeeType.getColor(BeeType.get(beTag.getString("beeType")), tintIndex);
                }
            }
            return color;
        }, BEE_NEST);
    }

    public static void onInitialize() {
        FuelRegistry.INSTANCE.add(COAL_COKE, 3200);
    }
}
