package zetabite.praunindustries.component;

import dev.onyxstudios.cca.api.v3.component.*;
import dev.onyxstudios.cca.api.v3.world.WorldComponentFactoryRegistry;
import dev.onyxstudios.cca.api.v3.world.WorldComponentInitializer;
import net.minecraft.util.Identifier;
import zetabite.praunindustries.machines.systems.CokeOvenManagerInstance;
import zetabite.praunindustries.util.ContentRegistryUtil;

import static zetabite.praunindustries.PraunIndustries.MODID;

public class ModComponents extends ContentRegistryUtil implements WorldComponentInitializer {
    public static final ComponentKey<ComponentV3> COKE_OVEN_MANAGER_COMPONENT = ComponentRegistryV3.INSTANCE.getOrCreate(new Identifier(MODID, "coke_oven_manager"), ComponentV3.class);

    @Override
    public void registerWorldComponentFactories(WorldComponentFactoryRegistry registry) {
        registry.register(COKE_OVEN_MANAGER_COMPONENT, CokeOvenManagerInstance::new);
    }
}
