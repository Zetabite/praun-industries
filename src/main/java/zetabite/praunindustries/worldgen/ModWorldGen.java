package zetabite.praunindustries.worldgen;

import zetabite.praunindustries.PraunIndustries;
import zetabite.praunindustries.bees.BeeType;
import zetabite.praunindustries.util.ContentRegistryUtil;
import zetabite.praunindustries.worldgen.feature.NestFeature;

import net.minecraft.util.Holder;
import net.minecraft.world.gen.GenerationStep;
import net.minecraft.world.gen.decorator.BiomePlacementModifier;
import net.minecraft.world.gen.decorator.CountPlacementModifier;
import net.minecraft.world.gen.decorator.RarityFilterPlacementModifier;
import net.minecraft.world.gen.feature.ConfiguredFeature;
import net.minecraft.world.gen.feature.PlacedFeature;
import net.minecraft.world.gen.feature.util.ConfiguredFeatureUtil;
import net.minecraft.world.gen.feature.util.PlacedFeatureUtil;

import org.quiltmc.qsl.worldgen.biome.api.BiomeModifications;
import org.quiltmc.qsl.worldgen.biome.api.BiomeSelectors;

import static zetabite.praunindustries.PraunIndustries.MODID;

public final class ModWorldGen extends ContentRegistryUtil {
    public static final NestFeature NEST = registerFeature("nest", new NestFeature());
    public static final Holder<ConfiguredFeature<NestFeature.NestFeatureConfig, ?>> NEST_CONFIGURED = ConfiguredFeatureUtil.register(
            MODID + ":nest",
            NEST,
            new NestFeature.NestFeatureConfig(BeeType.DEFAULT)
    );
    public static final Holder<PlacedFeature> NEST_PLACED = PlacedFeatureUtil.register(
            MODID + ":nest",
            NEST_CONFIGURED,
            BiomePlacementModifier.getInstance(),
            RarityFilterPlacementModifier.create(16),
            CountPlacementModifier.create(1),
            PlacedFeatureUtil.WORLD_SURFACE_WG_HEIGHTMAP
    );

    public static void onInitialize() {
        BiomeModifications.addFeature(BiomeSelectors.all(), GenerationStep.Feature.LOCAL_MODIFICATIONS, NEST_PLACED.method_40230().orElseThrow());
    }
}
