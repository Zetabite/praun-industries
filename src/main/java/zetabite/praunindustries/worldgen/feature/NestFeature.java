package zetabite.praunindustries.worldgen.feature;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import zetabite.praunindustries.bees.BeeType;
import zetabite.praunindustries.bees.block.entity.BeeNestBlockEntity;
import zetabite.praunindustries.block.ModBlocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.tag.BlockTags;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.Heightmap;
import net.minecraft.world.StructureWorldAccess;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.FeatureConfig;
import net.minecraft.world.gen.feature.util.FeatureContext;

public class NestFeature extends Feature<NestFeature.NestFeatureConfig> {
    public NestFeature() {
        super(NestFeatureConfig.CODEC);
    }

    @Override
    public boolean place(FeatureContext<NestFeatureConfig> ctx) {
        StructureWorldAccess world = ctx.getWorld();
        BlockPos pos = world.getTopPosition(Heightmap.Type.WORLD_SURFACE_WG, ctx.getOrigin());
        BlockState downState = world.getBlockState(pos.down());

        if (downState.isIn(BlockTags.DIRT)) {
            world.setBlockState(pos, ModBlocks.BEE_NEST.getDefaultState(), Block.NOTIFY_NEIGHBORS | Block.NOTIFY_LISTENERS);

            if (world.getBlockEntity(pos) instanceof BeeNestBlockEntity nest) {
                // TODO: The biome thingy?
                nest.setBeeType(ctx.getConfig().type);
            }
            return true;
        }

        return false;
    }

    public record NestFeatureConfig(BeeType type) implements FeatureConfig {
        public static final Codec<NestFeatureConfig> CODEC = RecordCodecBuilder.create(instance -> instance.group(
                Identifier.CODEC.fieldOf("type").xmap(BeeType::get, BeeType::getId).forGetter(NestFeatureConfig::type)
        ).apply(instance, NestFeatureConfig::new));
    }
}
