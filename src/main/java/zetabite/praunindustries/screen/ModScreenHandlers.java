package zetabite.praunindustries.screen;

// Minecraft / Mojang
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.gui.screen.ingame.HandledScreens;
import net.minecraft.screen.ScreenHandlerType;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

// Praun Industries
import zetabite.praunindustries.util.ModContentID;
import zetabite.praunindustries.machines.client.gui.screen.ingame.CokeOvenScreen;
import zetabite.praunindustries.machines.screen.CokeOvenScreenHandler;

import static zetabite.praunindustries.PraunIndustries.MODID;

public class ModScreenHandlers {
    public static final ScreenHandlerType<CokeOvenScreenHandler> COKE_OVEN;

    static {
        COKE_OVEN = Registry.register(Registry.SCREEN_HANDLER, new Identifier(MODID, ModContentID.COKE_OVEN), new ScreenHandlerType<>(CokeOvenScreenHandler::new));
    }

    public static void onInitialize() {
    }

    @Environment(EnvType.CLIENT)
    public static void onInitializeClient() {
        HandledScreens.register(COKE_OVEN, CokeOvenScreen::new);
    }
}
