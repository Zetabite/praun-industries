package zetabite.praunindustries.bees;

public record BeeTypeImpl(String name, int primary, int secondary, int honeyColor) implements BeeType {
    @Override
    public int getPrimary() {
        return primary;
    }

    @Override
    public int getSecondary() {
        return secondary;
    }

    @Override
    public int getHoneyColor() {
        return honeyColor;
    }

    @Override
    public String getName() {
        return name;
    }

    public static void onInitialize() {
    }
}
