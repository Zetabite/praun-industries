package zetabite.praunindustries.bees.client.render.entity;

import net.minecraft.client.render.entity.EntityRendererFactory;
import net.minecraft.client.render.entity.MobEntityRenderer;
import net.minecraft.client.render.entity.model.BeeEntityModel;
import net.minecraft.util.Identifier;
import zetabite.praunindustries.PraunIndustries;
import zetabite.praunindustries.bees.BeeType;
import zetabite.praunindustries.bees.entity.passive.ResourceBeeEntity;
import zetabite.praunindustries.entity.ModEntities;

public class ResourceBeeEntityRenderer extends MobEntityRenderer<ResourceBeeEntity, BeeEntityModel<ResourceBeeEntity>> {

    private static final Identifier ANTENNY_OVERLAY = new Identifier("praunindustries","textures/entity/bee/bee_antenna_overlay.png");
    private static final Identifier BODY_OVERLAY_0 = new Identifier("praunindustries","textures/entity/bee/bee_base_body_overlay_0.png");
    private static final Identifier BODY_OVERLAY_1 = new Identifier("praunindustries","textures/entity/bee/bee_base_body_overlay_1.png");
    private static final Identifier EYES_OVERLAY = new Identifier("praunindustries","textures/entity/bee/bee_eyes_overlay.png");
    private static final Identifier EYES_ANRGY_OVERLAY_0 = new Identifier("praunindustries","textures/entity/bee/bee_angry_eyes_overlay_0.png");
    private static final Identifier EYES_ANRGY_OVERLAY_1 = new Identifier("praunindustries","textures/entity/bee/bee_angry_eyes_overlay_1.png");
    private static final Identifier WINGS_OVERLAY_0 = new Identifier("praunindustries","textures/entity/bee/bee_wings_overlay_0.png");
    private static final Identifier WINGS_OVERLAY_1 = new Identifier("praunindustries","textures/entity/bee/bee_wings_overlay_1.png");
    private static final Identifier LEGS_OVERLAY = new Identifier("praunindustries","textures/entity/bee/bee_legs_overlay.png");
    private static final Identifier NECTAR_OVERLAY = new Identifier("praunindustries","textures/entity/bee/bee_nectar_overlay.png");
    private static final Identifier STINGER_OVERLAY = new Identifier("praunindustries","textures/entity/bee/bee_stinger_overlay.png");
    private Identifier ANGRY_TEXTURE = new Identifier("textures/entity/bee/bee_angry.png");
    private Identifier ANGRY_NECTAR_TEXTURE = new Identifier("textures/entity/bee/bee_angry_nectar.png");
    private Identifier PASSIVE_TEXTURE = new Identifier("textures/entity/bee/bee.png");
    private Identifier NECTAR_TEXTURE = new Identifier("textures/entity/bee/bee.png");

    public ResourceBeeEntityRenderer(EntityRendererFactory.Context context) {
        super(context, new BeeEntityModel(context.getPart(ModEntities.MODEL_RESOURCE_BEE_LAYER)), 0.4F);
    }

    public ResourceBeeEntityRenderer(EntityRendererFactory.Context context, BeeType beeType) {
        this(context, PraunIndustries.MODID, beeType);
    }

    public ResourceBeeEntityRenderer(EntityRendererFactory.Context context, String modid, BeeType beeType) {
        this(context);
        ANGRY_TEXTURE = new Identifier(modid, "textures/entity/bee/" + beeType.getName() + "_bee_angry.png");
        ANGRY_NECTAR_TEXTURE = new Identifier(modid, "textures/entity/bee/" + beeType.getName() + "_bee_angry_nectar.png");
        PASSIVE_TEXTURE = new Identifier(modid, "textures/entity/bee/" + beeType.getName() + "_bee.png");
        NECTAR_TEXTURE = new Identifier(modid, "textures/entity/bee/" + beeType.getName() + "_bee.png");
    }

    @Override
    public Identifier getTexture(ResourceBeeEntity entity) {
        if (entity.hasAngerTime()) {
            return entity.hasNectar() ? ANGRY_NECTAR_TEXTURE : ANGRY_TEXTURE;
        } else {
            return entity.hasNectar() ? NECTAR_TEXTURE : PASSIVE_TEXTURE;
        }
    }
}
