package zetabite.praunindustries.bees.entity.passive;

// Minecraft / Mojang
import net.minecraft.entity.EntityType;
import net.minecraft.entity.passive.BeeEntity;
import net.minecraft.world.World;

public class ResourceBeeEntity extends BeeEntity {
    public ResourceBeeEntity(EntityType<? extends BeeEntity> entityType, World world) {
        super(entityType, world);
    }
}
