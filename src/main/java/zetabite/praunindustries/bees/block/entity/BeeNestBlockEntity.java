package zetabite.praunindustries.bees.block.entity;

// Minecraft / Mojang
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BeehiveBlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.network.Packet;
import net.minecraft.network.listener.ClientPlayPacketListener;
import net.minecraft.network.packet.s2c.play.BlockEntityUpdateS2CPacket;
import net.minecraft.util.math.BlockPos;

// Other
import javax.annotation.Nullable;

// Praun Industries
import zetabite.praunindustries.bees.BeeType;
import zetabite.praunindustries.block.entity.ModBlockEntities;


public class BeeNestBlockEntity extends BeehiveBlockEntity {
    private final BlockEntityType<?> extType;
    private BeeType beeType;

    public BeeNestBlockEntity(BlockEntityType<?> type, BlockPos blockPos, BlockState blockState) {
        super(blockPos, blockState);
        this.extType = type;
    }

    public BeeNestBlockEntity(BlockPos blockPos, BlockState blockState) {
        this(ModBlockEntities.BEE_NEST, blockPos, blockState);
    }

    public void setBeeType(BeeType beeType) {
        this.beeType = beeType;
    }

    public BeeType getBeeType() {
        return beeType;
    }

    @Override
    protected void writeNbt(NbtCompound nbt) {
        super.writeNbt(nbt);
        writeType(nbt);
    }

    @Override
    public void readNbt(NbtCompound nbt) {
        super.readNbt(nbt);
        if (nbt.contains("beeType")) {
            setBeeType(BeeType.get(nbt.getString("beeType")));
        }
    }

    @Nullable
    @Override
    public Packet<ClientPlayPacketListener> toUpdatePacket() {
        return BlockEntityUpdateS2CPacket.of(this);
    }

    @Override
    public NbtCompound toInitialChunkDataNbt() {
        return writeType(super.toInitialChunkDataNbt());
    }

    @Override
    public BlockEntityType<?> getType() {
        return this.extType;
    }

    protected NbtCompound writeType(NbtCompound nbt) {
        if (beeType != null) nbt.putString("beeType", BeeType.getId(beeType).toString());
        return nbt;
    }
}
