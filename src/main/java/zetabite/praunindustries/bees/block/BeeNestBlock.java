package zetabite.praunindustries.bees.block;

// Minecraft / Mojang
import net.minecraft.block.BeehiveBlock;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BeehiveBlockEntity;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityTicker;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.BlockItem;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

// Other
import javax.annotation.Nullable;

// Praun Industries
import zetabite.praunindustries.bees.block.entity.BeeNestBlockEntity;

public class BeeNestBlock extends BeehiveBlock {
    public BeeNestBlock(Settings settings) {
        super(settings);
    }

    @Nullable
    @Override
    public BlockEntity createBlockEntity(BlockPos pos, BlockState state) {
        return new BeeNestBlockEntity(pos, state);
    }

    @Nullable
    @Override
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(World world, BlockState state, BlockEntityType<T> type) {
        return world.isClient ? null : (w, s, pos, be) -> BeehiveBlockEntity.serverTick(w, s, pos, (BeehiveBlockEntity) be);
    }

    @Override
    public void onPlaced(World world, BlockPos pos, BlockState state, @Nullable LivingEntity placer, ItemStack itemStack) {
        super.onPlaced(world, pos, state, placer, itemStack);
        BlockEntity be = world.getBlockEntity(pos);

        if (world.isClient && be instanceof BeeNestBlockEntity) {
            NbtCompound stackBeTag = BlockItem.getBlockEntityNbtFromStack(itemStack);

            if (stackBeTag != null) {
                if (be.copyItemDataRequiresOperator()) {
                    if (placer == null) return;
                    if (placer instanceof PlayerEntity player && !player.isCreativeLevelTwoOp()) return;
                }
                NbtCompound beTag = be.toNbt();
                NbtCompound copy = beTag.copy();
                beTag.copyFrom(stackBeTag);

                if (!beTag.equals(copy)) {
                    be.readNbt(beTag);
                    be.markDirty();
                }
            }
        }

    }
}
