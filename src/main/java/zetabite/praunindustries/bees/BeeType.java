package zetabite.praunindustries.bees;

// Minecraft / Mojang
import net.minecraft.util.Identifier;

// Other
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import javax.annotation.Nullable;
import javax.annotation.Nonnull;

// Praun Industries
import zetabite.praunindustries.PraunIndustries;

public interface BeeType {
    BiMap<Identifier, BeeType> TYPES = HashBiMap.create();

    BeeType DEFAULT = register("default", 0xFFFFFF, 0xFFFFFF, 0x000000);
    BeeType ROCKY = register("rocky", 0x7f7f7f, 0x686868, 0x8f8f8f);
    BeeType COAL = register("coal", 0x393c36, 0x252525, 0x252525);
    BeeType IRON = register("iron", 0xaf8e77, 0x77674f, 0xe2c0aa);
    BeeType GOLD = register("gold", 0xfcee4b, 0x9c7020, 0xffffb5);

    int getPrimary();

    int getSecondary();

    int getHoneyColor();

    String getName();

    static int getColor(BeeType type, int tintIndex) {
        if (type == null) type = DEFAULT;

        return (switch (tintIndex) {
            case 0 -> type.getPrimary();
            case 1 -> type.getSecondary();
            case 2 -> type.getHoneyColor();
            default -> throw new IllegalStateException("Unexpected value for tintIndex " + tintIndex);
        });
    }

    @Nonnull
    static Identifier getId(BeeType type) {
        Identifier id = TYPES.inverse().get(type);

        if (id == null) throw new IllegalStateException("BeeType %s was not registered!".formatted(type));
        return id;
    }

    static BeeType register(String name, int primary, int secondary, int honeyColor) {
        BeeTypeImpl type = new BeeTypeImpl(name, primary, secondary, honeyColor);
        Identifier id = new Identifier(PraunIndustries.MODID, name);
        TYPES.put(id, type);
        PraunIndustries.registerBaseGenome(id);
        return type;
    }

    @Nullable
    static BeeType get(Identifier id) {
        return TYPES.get(id);
    }

    @Nullable
    static BeeType get(String id) {
        return TYPES.get(new Identifier(id));
    }
}
