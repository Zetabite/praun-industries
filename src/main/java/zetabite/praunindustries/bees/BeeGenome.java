package zetabite.praunindustries.bees;

// Minecraft / Mojang
import net.minecraft.nbt.NbtCompound;
import net.minecraft.util.Identifier;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

public class BeeGenome {
    private final BiMap<Identifier, Integer> distribution;
    private final BeeType beeType;

    public BeeGenome(BeeType beeType) {
        this(BeeType.getId(beeType));
    }

    public BeeGenome(Identifier id) {
        this.distribution = getDistribution(id);
        this.beeType = getBeeType(distribution);
    }

    public BeeGenome(BiMap<Identifier, Integer> distribution) {
        this.distribution = distribution;
        this.beeType = getBeeType(distribution);
    }

    public BeeGenome(BeeGenome parent1, BeeGenome parent2) {
        this.distribution = getDistribution(parent1.getDistribution(), parent2.getDistribution());
        this.beeType = getBeeType(distribution);
    }

    public BeeGenome(BeeType beeType, BiMap<Identifier, Integer> distribution) {
        this.beeType = beeType;
        this.distribution = distribution;
    }

    public static BiMap<Identifier, Integer> getDistribution(BiMap<Identifier, Integer> parent1, BiMap<Identifier, Integer> parent2) {
        BiMap<Identifier, Integer> child = HashBiMap.create();
        int value;
        for (Identifier key : parent1.keySet()) {
            value = parent1.get(key);
            if (parent2.containsKey(key)) {
                value += parent2.get(key);
            }
            child.put(key, (int) Math.floor(((double) value) / 2));
        }
        for (Identifier key : parent2.keySet()) {
            if (!parent1.containsKey(key)) {
                child.put(key, (int) Math.floor(((double) parent2.get(key)) / 2));
            }
        }
        return child;
    }

    public static BiMap<Identifier, Integer> getDistribution(Identifier id) {
        BiMap<Identifier, Integer> child = HashBiMap.create();
        child.put(id, 100);
        return child;
    }

    public BiMap<Identifier, Integer> getDistribution() {
        BiMap<Identifier, Integer> duplicate = HashBiMap.create();
        for (Identifier key : distribution.keySet()) {
            duplicate.put(key, distribution.get(key));
        }
        return duplicate;
    }

    public static BeeType getBeeType(BiMap<Identifier, Integer> distribution) {
        BeeType beeType = BeeType.DEFAULT;
        int highest = 0;
        int current;

        for (Identifier key : distribution.keySet()) {
            current = distribution.get(key);

            if (current > highest) {
                highest = current;
                beeType = BeeType.get(key);
            } else {
                if (current == 50 && highest == 50) {
                    return beeType;
                }
            }
        }
        return beeType;
    }

    public BeeType getBeeType() {
        return beeType;
    }

    public static BeeGenome readBeeGenomeNbt(NbtCompound nbt) {
        if (nbt.contains("beeGenome")) {
            NbtCompound genomeNbt = nbt.getCompound("beeGenome");
            BeeType beeType = null;

            if (genomeNbt.contains("beeType")) {
                beeType = BeeType.get(genomeNbt.getString("beeType"));
            }
            BiMap<Identifier, Integer> distribution = HashBiMap.create();

            if (genomeNbt.contains("beeGenomeDistro")) {
                NbtCompound distroNbt = genomeNbt.getCompound("beeGenomeDistro");
                for (String key : distroNbt.getKeys()) {
                    distribution.put(new Identifier(key), distroNbt.getInt(key));
                }
            }
            if (beeType == null && !distribution.isEmpty()) {
                return new BeeGenome(distribution);
            } else if (beeType != null) {
                if (distribution.isEmpty()) {
                    return new BeeGenome(beeType);
                }
                return new BeeGenome(beeType, distribution);
            }
        }
        return null;
    }

    public static NbtCompound writeBeeGenomeNbt(NbtCompound nbt, BeeGenome beeGenome) {
        if (beeGenome != null) {
            NbtCompound distroNbt = new NbtCompound();
            BiMap<Identifier, Integer> distribution = beeGenome.getDistribution();

            for (Identifier key : distribution.keySet()) {
                distroNbt.putInt(key.toString(), distribution.get(key));
            }
            NbtCompound genomeNbt = new NbtCompound();
            genomeNbt.putString("beeType", BeeType.getId(beeGenome.getBeeType()).toString());
            genomeNbt.put("beeGenomeDistro", distroNbt);
            nbt.put("beeGenome", genomeNbt);
        }
        return nbt;
    }
}
