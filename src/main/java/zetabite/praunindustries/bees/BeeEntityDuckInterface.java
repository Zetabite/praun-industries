package zetabite.praunindustries.bees;

public interface BeeEntityDuckInterface {
    BeeGenome getBeeGenome();
    void setBeeGenome(BeeGenome beeGenome);
}
