package zetabite.praunindustries.recipe;

import net.minecraft.recipe.RecipeType;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import org.quiltmc.qsl.recipe.api.serializer.QuiltRecipeSerializer;
import zetabite.praunindustries.util.ModContentID;
import zetabite.praunindustries.machines.recipe.CokeOvenRecipe;
import zetabite.praunindustries.machines.recipe.CokeOvenRecipeSerializer;
import zetabite.praunindustries.util.ContentRegistryUtil;

import static zetabite.praunindustries.PraunIndustries.MODID;
import static zetabite.praunindustries.machines.block.entity.CokeOvenControllerEntity.STANDARD_PRODUCTION_TIME;
import static zetabite.praunindustries.machines.block.entity.CokeOvenControllerEntity.STANDARD_CREOSOTE_PRODUCTION;

public class ModRecipes extends ContentRegistryUtil {
    public static final RecipeType<CokeOvenRecipe> COKE_OVEN_RECIPE_TYPE;
    public static final QuiltRecipeSerializer<CokeOvenRecipe> COKE_OVEN_RECIPE_SERIALIZER;

    static {
        COKE_OVEN_RECIPE_TYPE = Registry.register(Registry.RECIPE_TYPE, new Identifier(MODID, ModContentID.COKE_OVEN), new RecipeType<CokeOvenRecipe>() {});
        COKE_OVEN_RECIPE_SERIALIZER = Registry.register(Registry.RECIPE_SERIALIZER, new Identifier(MODID, ModContentID.COKE_OVEN), new CokeOvenRecipeSerializer<>(CokeOvenRecipe::new, STANDARD_PRODUCTION_TIME, STANDARD_CREOSOTE_PRODUCTION));
    }

    public static void onInitialize() {
    }
}
