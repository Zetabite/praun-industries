package zetabite.praunindustries.util;

// Minecraft / Mojang
import net.minecraft.block.Block;
import net.minecraft.fluid.FlowableFluid;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.gen.feature.Feature;

// Quilt
import org.quiltmc.qsl.item.setting.api.QuiltItemSettings;

// Praun Industries

import static zetabite.praunindustries.PraunIndustries.MODID;
import static zetabite.praunindustries.PraunIndustries.ITEM_GROUP;

public abstract class ContentRegistryUtil {
    public static <B extends Block> BlockItem registerBlockItem(String id, B block) {
        return Registry.register(Registry.ITEM, new Identifier(MODID, id), new BlockItem(block, new QuiltItemSettings()));
    }
    public static <B extends Block> BlockItem registerBlockItem(String id, B block, ItemGroup itemGroup) {
        return Registry.register(Registry.ITEM, new Identifier(MODID, id), new BlockItem(block, new QuiltItemSettings().group(itemGroup)));
    }

    public static Item registerItem(String id) {
        return registerItem(id, new Item(new QuiltItemSettings()));
    }

    public static Item registerItem(String id, ItemGroup itemGroup) {
        return registerItem(id, new Item(new QuiltItemSettings().group(itemGroup)));
    }

    public static <I extends Item> Item registerItem(String id, I item) {
        return Registry.register(Registry.ITEM, new Identifier(MODID, id), item);
    }

    public static <B extends Block> Block registerBlock(String id, B value) {
        return Registry.register(Registry.BLOCK, new Identifier(MODID, id), value);
    }

    public static FlowableFluid registerFlowableFluid(String id, FlowableFluid fluid) {
        return Registry.register(Registry.FLUID, new Identifier(MODID, id), fluid);
    }

    public static <T extends Feature<?>> T registerFeature(String id, T feature) {
        return Registry.register(Registry.FEATURE, new Identifier(MODID, id), feature);
    }
}
