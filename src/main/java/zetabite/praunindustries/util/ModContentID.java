package zetabite.praunindustries.util;

public class ModContentID {

    public static final String COKE_OVEN = "coke_oven";
    public static final String COKE_OVEN_BRICK = COKE_OVEN + "_brick";
    public static final String COKE_OVEN_PERIPHERAL = COKE_OVEN + "_peripheral";
    public static final String COKE_OVEN_CONTROLLER = COKE_OVEN + "_controller";
    public static final String COAL_COKE = "coal_coke";
    public static final String BEE_NEST = "bee_nest";
    public static final String CREOSOTE = "creosote";
    public static final String RESOURCE_BEE = "resource_bee";
}
