package zetabite.praunindustries.machines.block;

import net.minecraft.block.Block;
import net.minecraft.block.BlockRenderType;
import net.minecraft.block.BlockState;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.World;
import zetabite.praunindustries.block.ModBlocks;

import javax.annotation.Nullable;

import static zetabite.praunindustries.machines.systems.CokeOvenManager.COKE_OVEN_MANAGER;

public class CokeOvenBrick extends Block {

    public CokeOvenBrick(Settings settings) {
        super(settings);
    }

    @Override
    public void onPlaced(World world, BlockPos pos, BlockState state, @Nullable LivingEntity placer, ItemStack itemStack) {
        Direction facing = ModBlocks.COKE_OVEN_PERIPHERAL.getDefaultState().get(CokeOvenPeripheral.FACING);

        if (placer != null) {
            facing = placer.getHorizontalFacing().getOpposite();
        }
        COKE_OVEN_MANAGER.findAndAttemptToCreateCokeOven(world, pos, facing);
    }

    @Override
    public BlockRenderType getRenderType(BlockState state) {
        return BlockRenderType.MODEL;
    }

    public static void replace(BlockState state, BlockState newState, World world, BlockPos pos, int flags) {
        Block.replace(state, newState, world, pos, flags, 512);
        COKE_OVEN_MANAGER.findAndAttemptToCreateCokeOven(world, pos, ModBlocks.COKE_OVEN_PERIPHERAL.getDefaultState().get(CokeOvenPeripheral.FACING));
    }
}
