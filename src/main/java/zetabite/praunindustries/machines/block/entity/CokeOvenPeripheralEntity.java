package zetabite.praunindustries.machines.block.entity;

// Minecraft / Mojang
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.SidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.util.math.BlockPos;

import net.minecraft.util.math.Direction;
import org.jetbrains.annotations.Nullable;
import zetabite.praunindustries.block.entity.ModBlockEntities;

import static zetabite.praunindustries.machines.systems.CokeOvenManager.COKE_OVEN_MANAGER;

public class CokeOvenPeripheralEntity extends BlockEntity implements SidedInventory {
    public CokeOvenPeripheralEntity(BlockPos blockPos, BlockState blockState) {
        super(ModBlockEntities.COKE_OVEN_PERIPHERAL_ENTITY, blockPos, blockState);
    }

    // Write
    @Override
    public void writeNbt(NbtCompound tag) {
        super.writeNbt(tag);
    }

    // Read
    @Override
    public void readNbt(NbtCompound tag) {
        super.readNbt(tag);
        COKE_OVEN_MANAGER.scheduleForDestruction(world, getPos());
    }

    @Override
    public int[] getAvailableSlots(Direction side) {
        CokeOvenControllerEntity cokeOvenControllerEntity = COKE_OVEN_MANAGER.getCokeOvenController(this.world, this.getPos());

        if (cokeOvenControllerEntity != null) return cokeOvenControllerEntity.getAvailableSlots(side);
        return new int[0];
    }

    @Override
    public boolean canInsert(int slot, ItemStack stack, @Nullable Direction dir) {
        CokeOvenControllerEntity cokeOvenControllerEntity = COKE_OVEN_MANAGER.getCokeOvenController(this.world, this.getPos());

        if (cokeOvenControllerEntity != null) return cokeOvenControllerEntity.canInsert(slot, stack, dir);
        return false;
    }

    @Override
    public boolean canExtract(int slot, ItemStack stack, Direction dir) {
        CokeOvenControllerEntity cokeOvenControllerEntity = COKE_OVEN_MANAGER.getCokeOvenController(this.world, this.getPos());

        if (cokeOvenControllerEntity != null) return cokeOvenControllerEntity.canExtract(slot, stack, dir);
        return false;
    }

    @Override
    public int size() {
        CokeOvenControllerEntity cokeOvenControllerEntity = COKE_OVEN_MANAGER.getCokeOvenController(this.world, this.getPos());

        if (cokeOvenControllerEntity != null) return cokeOvenControllerEntity.size();
        return 0;
    }

    @Override
    public boolean isEmpty() {
        CokeOvenControllerEntity cokeOvenControllerEntity = COKE_OVEN_MANAGER.getCokeOvenController(this.world, this.getPos());

        if (cokeOvenControllerEntity != null) return cokeOvenControllerEntity.isEmpty();
        return false;
    }

    @Override
    public ItemStack getStack(int slot) {
        CokeOvenControllerEntity cokeOvenControllerEntity = COKE_OVEN_MANAGER.getCokeOvenController(this.world, this.getPos());

        if (cokeOvenControllerEntity != null) return cokeOvenControllerEntity.getStack(slot);
        return null;
    }

    @Override
    public ItemStack removeStack(int slot, int amount) {
        CokeOvenControllerEntity cokeOvenControllerEntity = COKE_OVEN_MANAGER.getCokeOvenController(this.world, this.getPos());

        if (cokeOvenControllerEntity != null) return cokeOvenControllerEntity.removeStack(slot, amount);
        return null;
    }

    @Override
    public ItemStack removeStack(int slot) {
        CokeOvenControllerEntity cokeOvenControllerEntity = COKE_OVEN_MANAGER.getCokeOvenController(this.world, this.getPos());

        if (cokeOvenControllerEntity != null) return cokeOvenControllerEntity.removeStack(slot);
        return null;
    }

    @Override
    public void setStack(int slot, ItemStack stack) {
        CokeOvenControllerEntity cokeOvenControllerEntity = COKE_OVEN_MANAGER.getCokeOvenController(this.world, this.getPos());

        if (cokeOvenControllerEntity != null) cokeOvenControllerEntity.setStack(slot, stack);
    }

    @Override
    public boolean canPlayerUse(PlayerEntity player) {
        CokeOvenControllerEntity cokeOvenControllerEntity = COKE_OVEN_MANAGER.getCokeOvenController(this.world, this.getPos());

        if (cokeOvenControllerEntity != null) return cokeOvenControllerEntity.canPlayerUse(player);
        return false;
    }

    @Override
    public void clear() {
        CokeOvenControllerEntity cokeOvenControllerEntity = COKE_OVEN_MANAGER.getCokeOvenController(this.world, this.getPos());

        if (cokeOvenControllerEntity != null) cokeOvenControllerEntity.clear();
    }
}