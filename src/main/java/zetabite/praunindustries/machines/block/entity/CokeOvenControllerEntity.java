package zetabite.praunindustries.machines.block.entity;

// Minecraft / Mojang

import com.google.common.collect.Lists;
import it.unimi.dsi.fastutil.objects.Object2IntMap;
import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.LockableContainerBlockEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventories;
import net.minecraft.inventory.SidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.recipe.*;
import net.minecraft.screen.PropertyDelegate;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Identifier;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import zetabite.praunindustries.util.ModConstants;
import zetabite.praunindustries.util.ModContentID;
import zetabite.praunindustries.block.entity.ModBlockEntities;
import zetabite.praunindustries.machines.block.CokeOvenController;
import zetabite.praunindustries.machines.block.CokeOvenPeripheral;
import zetabite.praunindustries.machines.recipe.CokeOvenRecipe;
import zetabite.praunindustries.machines.screen.CokeOvenScreenHandler;
import zetabite.praunindustries.recipe.ModRecipes;

import java.util.List;

import static zetabite.praunindustries.PraunIndustries.MODID;
import static zetabite.praunindustries.machines.systems.CokeOvenManager.*;

public class CokeOvenControllerEntity extends LockableContainerBlockEntity implements SidedInventory, RecipeUnlocker, RecipeInputProvider {
    private static final int INPUT_SLOT = 0;
    private static final int OUTPUT_SLOT = 1;
    public static final int SLOTS_COUNT = 2;
    private static final int[] TOP_SLOTS = new int[]{INPUT_SLOT};
    private static final int[] BOTTOM_SLOTS = new int[]{OUTPUT_SLOT};
    private static final int[] SIDE_SLOTS = new int[]{INPUT_SLOT};
    public static final int COOK_TIME_DATA = 0;
    public static final int TOTAL_COOK_TIME_DATA = 1;
    public static final int CREOSOTE_LEVEL_DATA = 2;
    public static final int IS_BURNING_DATA = 3;
    public static final int DATA_VALUES = 4;
    public static final int STANDARD_PRODUCTION_TIME = 500;
    public static final long STANDARD_CREOSOTE_PRODUCTION = 0;
    private DefaultedList<ItemStack> inventory = DefaultedList.ofSize(SLOTS_COUNT, ItemStack.EMPTY);
    int cookTime = 0;
    int cookTimeTotal = 0;
    long creosoteLevel = 0;
    boolean isBurning = false;
    public static final long MAX_CREOSOTE_LEVEL = ModConstants.bucketMilliBucketSize * 15;
    private final PropertyDelegate propertyDelegate = new PropertyDelegate() {
        @Override
        public int get(int index) {
            return switch (index) {
                case COOK_TIME_DATA -> cookTime;
                case TOTAL_COOK_TIME_DATA -> cookTimeTotal;
                case CREOSOTE_LEVEL_DATA -> (int) creosoteLevel;
                case IS_BURNING_DATA -> isBurning ? 1 : 0;
                default -> COOK_TIME_DATA;
            };
        }

        @Override
        public void set(int index, int value) {
            switch (index) {
                case COOK_TIME_DATA -> cookTime = value;
                case TOTAL_COOK_TIME_DATA -> cookTimeTotal = value;
                case CREOSOTE_LEVEL_DATA -> creosoteLevel = MathHelper.clamp(value, 0, MAX_CREOSOTE_LEVEL);
                case IS_BURNING_DATA -> isBurning = value > 0;
            }
        }

        @Override
        public int size() {
            return DATA_VALUES;
        }
    };
    private final Object2IntOpenHashMap<Identifier> recipesUsed = new Object2IntOpenHashMap<>();
    private final RecipeType<? extends CokeOvenRecipe> recipeType = ModRecipes.COKE_OVEN_RECIPE_TYPE;

    public CokeOvenControllerEntity(BlockPos blockPos, BlockState blockState) {
        super(ModBlockEntities.COKE_OVEN_CONTROLLER_ENTITY, blockPos, blockState);
    }

    private boolean isBurning() {
        return isBurning;
    }

    private boolean hasMaxCreosote() {
        return creosoteLevel == MAX_CREOSOTE_LEVEL;
    }

    public long getCreosoteLevel() {
        return creosoteLevel;
    }

    public boolean tryRemoveFluidAmount(long amount) {
        if (amount <= creosoteLevel) {
            creosoteLevel -= amount;
            return true;
        }
        return false;
    }

    public boolean couldRemoveFluidAmount(long amount) {
        return amount <= creosoteLevel;
    }

    public boolean tryAddFluidAmount(long amount) {
        if (creosoteLevel + amount <= MAX_CREOSOTE_LEVEL) {
            creosoteLevel += amount;
            return true;
        }
        return false;
    }

    public boolean couldAddFluidAmount(long amount) {
        return amount + creosoteLevel <= MAX_CREOSOTE_LEVEL;
    }

    @Override
    public void readNbt(NbtCompound tag) {
        super.readNbt(tag);
        inventory = DefaultedList.ofSize(size(), ItemStack.EMPTY);
        Inventories.readNbt(tag, inventory);
        cookTime = tag.getShort("CookTime");
        creosoteLevel = tag.getLong("CreosoteLevel");
        cookTimeTotal = tag.getShort("CookTimeTotal");

        NbtCompound recipeUsedCompound = tag.getCompound("RecipesUsed");

        for(String string : recipeUsedCompound.getKeys()) {
            recipesUsed.put(new Identifier(string), recipeUsedCompound.getInt(string));
        }
        COKE_OVEN_MANAGER.scheduleForDestruction(world, getPos());
    }

    @Override
    public void writeNbt(NbtCompound tag) {
        super.writeNbt(tag);
        tag.putShort("CookTime", (short) cookTime);
        tag.putShort("CookTimeTotal", (short) cookTimeTotal);
        tag.putLong("CreosoteLevel", creosoteLevel);
        Inventories.writeNbt(tag, inventory);

        NbtCompound recipeUsedCompound = new NbtCompound();
        recipesUsed.forEach((identifier, count) -> recipeUsedCompound.putInt(identifier.toString(), count));
        tag.put("RecipesUsed", recipeUsedCompound);
    }

    private static boolean canAcceptRecipeOutput(@Nullable Recipe<?> recipe, DefaultedList<ItemStack> slots, int count) {
        if (!slots.get(INPUT_SLOT).isEmpty() && recipe != null) {
            ItemStack recipeOutputStack = recipe.getOutput();

            if (recipeOutputStack.isEmpty()) {
                return false;
            } else {
                ItemStack outputStack = slots.get(OUTPUT_SLOT);
                if (outputStack.isEmpty()) {
                    return true;
                } else if (!outputStack.isItemEqualIgnoreDamage(recipeOutputStack)) {
                    return false;
                } else if (outputStack.getCount() < count && outputStack.getCount() < outputStack.getMaxCount()) {
                    return true;
                } else {
                    return outputStack.getCount() < recipeOutputStack.getMaxCount();
                }
            }
        } else {
            return false;
        }
    }

    private static boolean craftRecipe(CokeOvenControllerEntity controllerEntity, @Nullable Recipe<?> recipe, int count) {
        DefaultedList<ItemStack> slots = controllerEntity.inventory;

        if (recipe != null && canAcceptRecipeOutput(recipe, slots, count)) {
            ItemStack inputSlotStack = slots.get(INPUT_SLOT);
            ItemStack recipeOutputStack = recipe.getOutput();
            ItemStack outputSlotStack = slots.get(OUTPUT_SLOT);

            if (outputSlotStack.isEmpty()) {
                slots.set(OUTPUT_SLOT, recipeOutputStack.copy());
            } else if (outputSlotStack.isOf(recipeOutputStack.getItem())) {
                outputSlotStack.increment(1);
            }
            inputSlotStack.decrement(1);

            if (recipe instanceof CokeOvenRecipe cokeOvenRecipe) {
                long potentialCreosoteAdded = cokeOvenRecipe.getCreosoteAdded();
                controllerEntity.creosoteLevel = MathHelper.clamp(controllerEntity.creosoteLevel + potentialCreosoteAdded, 0, MAX_CREOSOTE_LEVEL);
            }
            return true;
        } else {
            return false;
        }
    }

    private static int getCookTime(World world, CokeOvenControllerEntity controllerEntity) {
        return world.getRecipeManager().getFirstMatch(controllerEntity.recipeType, controllerEntity, world).map(CokeOvenRecipe::getCookTime).orElse(STANDARD_PRODUCTION_TIME);
    }

    @Override
    public int[] getAvailableSlots(Direction side) {
        if (side == Direction.DOWN) {
            return BOTTOM_SLOTS;
        } else {
            return side == Direction.UP ? TOP_SLOTS : SIDE_SLOTS;
        }
    }

    // TODO: Redirect in Peripheral Entity
    @Override
    public boolean canInsert(int slot, ItemStack stack, @Nullable Direction dir) {
        return isValid(slot, stack);
    }

    @Override
    public boolean canExtract(int slot, ItemStack stack, Direction dir) {
        return dir == Direction.DOWN && slot == OUTPUT_SLOT;
    }

    @Override
    public int size() {
        return inventory.size();
    }

    @Override
    public boolean isEmpty() {
        for(ItemStack itemStack : inventory) {
            if (!itemStack.isEmpty()) {
                return false;
            }
        }
        return true;
    }

    @Override
    public ItemStack getStack(int slot) {
        return inventory.get(slot);
    }

    @Override
    public ItemStack removeStack(int slot, int amount) {
        return Inventories.splitStack(inventory, slot, amount);
    }

    @Override
    public ItemStack removeStack(int slot) {
        return Inventories.removeStack(inventory, slot);
    }

    @Override
    public void setStack(int slot, ItemStack stack) {
        ItemStack itemStack = inventory.get(slot);
        boolean matchCondition = !stack.isEmpty() && stack.isItemEqualIgnoreDamage(itemStack) && ItemStack.areNbtEqual(stack, itemStack);
        inventory.set(slot, stack);

        if (stack.getCount() > getMaxCountPerStack()) {
            stack.setCount(getMaxCountPerStack());
        }

        if (slot == INPUT_SLOT && !matchCondition) {
            cookTimeTotal = getCookTime(world, this);
            cookTime = 0;
            markDirty();
        }
    }

    @Override
    public boolean canPlayerUse(PlayerEntity player) {
        if (world == null || world.getBlockEntity(pos) != this) {
            return false;
        } else {
            return player.squaredDistanceTo((double) pos.getX() + 0.5, (double) pos.getY() + 0.5, (double) pos.getZ() + 0.5) <= 64.0;
        }
    }

    @Override
    public boolean isValid(int slot, ItemStack stack) {
        return slot != OUTPUT_SLOT;
    }

    @Override
    public void clear() {
        inventory.clear();
    }

    @Override
    public void setLastRecipe(@Nullable Recipe<?> recipe) {
        if (recipe == null) return;
        Identifier identifier = recipe.getId();
        recipesUsed.addTo(identifier, 1);
    }

    @Nullable
    @Override
    public Recipe<?> getLastRecipe() {
        return null;
    }

    @Override
    public void unlockLastRecipe(PlayerEntity player) {
    }

    public void unlockRecipes(ServerPlayerEntity player) {
        List<Recipe<?>> list = getRecipesUsed(player.getWorld());
        player.unlockRecipes(list);
        recipesUsed.clear();
    }

    public List<Recipe<?>> getRecipesUsed(ServerWorld world) {
        List<Recipe<?>> list = Lists.newArrayList();

        for(Object2IntMap.Entry<Identifier> entry : recipesUsed.object2IntEntrySet()) {
            world.getRecipeManager().get(entry.getKey()).ifPresent(list::add);
        }
        return list;
    }

    @Override
    public void provideRecipeInputs(RecipeMatcher finder) {
        for(ItemStack itemStack : inventory) {
            finder.addInput(itemStack);
        }
    }

    @Override
    public Text getContainerName() {
        return new TranslatableText("container." + MODID + "." + ModContentID.COKE_OVEN);
    }

    @Override
    public Text getDisplayName() {
        return getContainerName();
    }

    @Override
    public ScreenHandler createScreenHandler(int syncId, PlayerInventory playerInventory) {
        return new CokeOvenScreenHandler(syncId, playerInventory, this, this.propertyDelegate);
    }

    public static void tick(World world, BlockPos pos, BlockState state, CokeOvenControllerEntity controllerEntity) {
        boolean isMarkedDirty = false;
        boolean wasBurningAtTickStart = controllerEntity.isBurning();

        if (controllerEntity.isBurning() || !controllerEntity.inventory.get(INPUT_SLOT).isEmpty()) {
            Recipe<?> recipe = world.getRecipeManager().getFirstMatch(ModRecipes.COKE_OVEN_RECIPE_TYPE, controllerEntity, world).orElse(null);
            int maxCountPerStack = controllerEntity.getMaxCountPerStack();

            if (!controllerEntity.isBurning() && canAcceptRecipeOutput(recipe, controllerEntity.inventory, maxCountPerStack)) {
                controllerEntity.isBurning = true;
                controllerEntity.cookTime = 0;
                controllerEntity.cookTimeTotal = getCookTime(world, controllerEntity);
            }

            if (controllerEntity.isBurning() && canAcceptRecipeOutput(recipe, controllerEntity.inventory, maxCountPerStack)) {
                controllerEntity.cookTime++;

                if (controllerEntity.cookTime == controllerEntity.cookTimeTotal) {
                    controllerEntity.cookTime = 0;
                    controllerEntity.cookTimeTotal = getCookTime(world, controllerEntity);

                    if (craftRecipe(controllerEntity, recipe, maxCountPerStack)) {
                        controllerEntity.setLastRecipe(recipe);
                    }
                    isMarkedDirty = true;
                }
            } else {
                controllerEntity.cookTime = 0;
                controllerEntity.isBurning = false;
            }
        } else {
            controllerEntity.cookTime = 0;
            controllerEntity.isBurning = false;
        }

        if (controllerEntity.inventory.get(INPUT_SLOT).isEmpty()) {
            controllerEntity.isBurning = false;
        }

        if ((wasBurningAtTickStart != controllerEntity.isBurning())) {
            isMarkedDirty = true;
            BlockPos.Mutable interfacePos = new BlockPos.Mutable();
            int x = pos.getX();
            int y = pos.getY();
            int z = pos.getZ();

            for (int i = 0; i < 4; i++) {
                interfacePos.set(x + INTERFACE_OFFSETS[i][0], y, z + INTERFACE_OFFSETS[i][1]);
                BlockState interfaceState = world.getBlockState(interfacePos);
                interfaceState = interfaceState.with(CokeOvenPeripheral.LIT, controllerEntity.isBurning());
                world.setBlockState(interfacePos, interfaceState, 3);
                markDirty(world, interfacePos, interfaceState);
            }
            state = state.with(CokeOvenController.LIT, controllerEntity.isBurning());
        }

        if (isMarkedDirty) {
            markDirty(world, pos, state);
        }
    }
}