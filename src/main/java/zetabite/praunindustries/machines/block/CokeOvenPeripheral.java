package zetabite.praunindustries.machines.block;

// Minecraft / Mojang
import net.minecraft.advancement.criterion.Criteria;
import net.minecraft.block.*;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.*;
import net.minecraft.particle.ParticleTypes;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.stat.Stats;
import net.minecraft.state.StateManager;
import net.minecraft.state.property.BooleanProperty;
import net.minecraft.state.property.DirectionProperty;
import net.minecraft.state.property.Properties;
import net.minecraft.util.*;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.World;

// Other
import javax.annotation.Nullable;

// Praun Industries
import zetabite.praunindustries.block.ModBlocks;
import zetabite.praunindustries.fluid.CreosoteBottleItem;
import zetabite.praunindustries.fluid.CreosoteBucketItem;
import zetabite.praunindustries.fluid.ModFluids;
import zetabite.praunindustries.machines.block.entity.CokeOvenControllerEntity;
import zetabite.praunindustries.machines.block.entity.CokeOvenPeripheralEntity;
import zetabite.praunindustries.util.ModConstants;

import java.util.Random;

import static zetabite.praunindustries.machines.systems.CokeOvenManager.COKE_OVEN_MANAGER;

public class CokeOvenPeripheral extends BlockWithEntity {

    public static final DirectionProperty FACING = HorizontalFacingBlock.FACING;
    public static final BooleanProperty INTERFACE = BooleanProperty.of("interface");
    public static final BooleanProperty LIT = Properties.LIT;

    public CokeOvenPeripheral(Settings settings) {
        super(settings);
        this.setDefaultState(this.stateManager.getDefaultState().with(FACING, Direction.NORTH).with(LIT, false).with(INTERFACE, false));
    }

    @Override
    public ActionResult onUse(BlockState state, World world, BlockPos blockPos, PlayerEntity player, Hand hand, BlockHitResult hit) {
        if (world instanceof ServerWorld) {
            CokeOvenControllerEntity controllerEntity = COKE_OVEN_MANAGER.getCokeOvenController(world, blockPos);

            if (controllerEntity != null) {
                ItemStack stackInHand = player.getStackInHand(hand);

                if (!stackInHand.isEmpty()) {
                    if (stackInHand.getItem().asItem() instanceof CreosoteBucketItem creosoteBucketItem) {
                        if (creosoteBucketItem.equals(ModFluids.CREOSOTE_BUCKET) && controllerEntity.tryAddFluidAmount(ModConstants.bucketMilliBucketSize)) {
                            player.incrementStat(Stats.USED.getOrCreateStat(creosoteBucketItem));
                            world.playSound(null, player.getBlockPos(), SoundEvents.ITEM_BUCKET_EMPTY, SoundCategory.NEUTRAL, 1.0F, 1.0F);
                            //world.emitGameEvent(player, GameEvent.FLUID_PLACE, blockPos);
                            stackInHand.decrement(1);
                            ItemStack bucketStack = new ItemStack(Items.BUCKET, 1);

                            if (!player.getInventory().insertStack(bucketStack)) {
                                ItemScatterer.spawn(world, blockPos.getX(), blockPos.getY(), blockPos.getZ(), bucketStack);
                            }
                            return ActionResult.SUCCESS;
                        }
                    } else if (stackInHand.getItem().asItem() instanceof BucketItem bucketItem) {
                        if (bucketItem.equals(Items.BUCKET) && controllerEntity.tryRemoveFluidAmount(ModConstants.bucketMilliBucketSize)) {
                            player.incrementStat(Stats.USED.getOrCreateStat(bucketItem));
                            world.playSound(null, player.getBlockPos(), SoundEvents.ITEM_BUCKET_FILL, SoundCategory.NEUTRAL, 1.0F, 1.0F);
                            //world.emitGameEvent(player, GameEvent.FLUID_PICKUP, blockPos);
                            stackInHand.decrement(1);
                            ItemStack creosoteBucketStack = new ItemStack(ModFluids.CREOSOTE_BUCKET, 1);

                            if (!player.getInventory().insertStack(creosoteBucketStack)) {
                                ItemScatterer.spawn(world, blockPos.getX(), blockPos.getY(), blockPos.getZ(), creosoteBucketStack);
                            }
                            Criteria.FILLED_BUCKET.trigger((ServerPlayerEntity) player, creosoteBucketStack);
                            return ActionResult.SUCCESS;
                        }
                    } else if (stackInHand.getItem().asItem() instanceof CreosoteBottleItem creosoteBottleItem) {
                        if (creosoteBottleItem.equals(ModFluids.CREOSOTE_BOTTLE) && controllerEntity.tryAddFluidAmount(ModConstants.bottleMilliBucketSize)) {
                            world.playSound(null, player.getBlockPos(), SoundEvents.ITEM_BOTTLE_EMPTY, SoundCategory.NEUTRAL, 1.0F, 1.0F);
                            //world.emitGameEvent(player, GameEvent.FLUID_PLACE, blockPos);
                            player.incrementStat(Stats.USED.getOrCreateStat(creosoteBottleItem));
                            stackInHand.decrement(1);
                            ItemStack glassBottleStack = new ItemStack(Items.GLASS_BOTTLE, 1);

                            if (!player.getInventory().insertStack(glassBottleStack)) {
                                ItemScatterer.spawn(world, blockPos.getX(), blockPos.getY(), blockPos.getZ(), glassBottleStack);
                            }
                            return ActionResult.SUCCESS;
                        }
                    } else if (stackInHand.getItem().asItem() instanceof GlassBottleItem glassBottleItem) {
                        if (glassBottleItem.equals(Items.GLASS_BOTTLE) && controllerEntity.tryRemoveFluidAmount(ModConstants.bottleMilliBucketSize)) {
                            world.playSound(null, player.getBlockPos(), SoundEvents.ITEM_BOTTLE_FILL, SoundCategory.NEUTRAL, 1.0F, 1.0F);
                            //world.emitGameEvent(player, GameEvent.FLUID_PICKUP, blockPos);
                            player.incrementStat(Stats.USED.getOrCreateStat(glassBottleItem));
                            stackInHand.decrement(1);
                            ItemStack creosoteBottleStack = new ItemStack(ModFluids.CREOSOTE_BOTTLE, 1);

                            if (!player.getInventory().insertStack(creosoteBottleStack)) {
                                ItemScatterer.spawn(world, blockPos.getX(), blockPos.getY(), blockPos.getZ(), creosoteBottleStack);
                            }
                            return ActionResult.SUCCESS;
                        }
                    }
                }
                CokeOvenController controller = (CokeOvenController) world.getBlockState(controllerEntity.getPos()).getBlock();
                controller.openScreen(world, controllerEntity.getPos(), player);
                return ActionResult.CONSUME;
            }
        }
        return ActionResult.SUCCESS;
    }

    @Override
    protected void appendProperties(StateManager.Builder<Block, BlockState> stateManager) {
        stateManager.add(INTERFACE, LIT, FACING);
    }

    @Override
    public BlockState getPlacementState(ItemPlacementContext ctx) {
        return this.getDefaultState().with(FACING, ctx.getPlayerFacing().getOpposite()).with(INTERFACE, false);
    }

    @Override
    public BlockRenderType getRenderType(BlockState state) {
        return BlockRenderType.MODEL;
    }

    @Override
    public void onPlaced(World world, BlockPos pos, BlockState state, @Nullable LivingEntity placer, ItemStack itemStack) {
        if (world instanceof ServerWorld) {
            CokeOvenPeripheral.replace(state, ModBlocks.COKE_OVEN_BRICK.getDefaultState(), world, pos, Block.NOTIFY_ALL);
        }
    }

    @Override
    public void onBreak(World world, BlockPos pos, BlockState state, PlayerEntity player) {
        COKE_OVEN_MANAGER.scheduleForDestruction(world, pos);
        super.onBreak(world, pos, state, player);
    }

    @Override
    public void onStateReplaced(BlockState state, World world, BlockPos pos, BlockState newState, boolean moved) {
        if (!state.isOf(newState.getBlock())) {

            if (world.getBlockEntity(pos) instanceof CokeOvenControllerEntity cokeOvenPartEntity) {
                if (world instanceof ServerWorld serverWorld) {
                    ItemScatterer.spawn(world, pos, cokeOvenPartEntity);
                    cokeOvenPartEntity.getRecipesUsed(serverWorld);
                }

                world.updateComparators(pos, this);
            }
            super.onStateReplaced(state, world, pos, newState, moved);
        }
    }

    @Override
    public boolean hasComparatorOutput(BlockState state) {
        return true;
    }

    @Override
    public int getComparatorOutput(BlockState state, World world, BlockPos pos) {
        return ScreenHandler.calculateComparatorOutput(world.getBlockEntity(pos));
    }

    @Override
    public BlockState rotate(BlockState state, BlockRotation rotation) {
        return state.with(FACING, rotation.rotate(state.get(FACING)));
    }

    @Override
    public BlockState mirror(BlockState state, BlockMirror mirror) {
        return state.rotate(mirror.getRotation(state.get(FACING)));
    }

    @Nullable
    @Override
    public BlockEntity createBlockEntity(BlockPos pos, BlockState state) {
        return new CokeOvenPeripheralEntity(pos, state);
    }

    @Override
    public void randomDisplayTick(BlockState state, World world, BlockPos pos, Random random) {
        if (state.get(LIT) && state.get(INTERFACE)) {
            double d = pos.getX() + 0.5;
            double e = pos.getY();
            double f = pos.getZ() + 0.5;

            if (random.nextDouble() < 0.1) {
                world.playSound(d, e, f, SoundEvents.BLOCK_FURNACE_FIRE_CRACKLE, SoundCategory.BLOCKS, 1.0F, 1.0F, false);
            }

            double xOffset = pos.getX() + 0.5;
            double yOffset = pos.getY() + 0.5;
            double zOffset = pos.getZ() + 0.5;

            Direction direction = state.get(FACING);
            Direction.Axis axis = direction.getAxis();
            double g = 0.52;
            double h = random.nextDouble() * 0.6 - 0.3;
            double i = axis == Direction.Axis.X ? direction.getOffsetX() * g : h;
            double j = random.nextDouble() * 6.0 / 16.0;
            double k = axis == Direction.Axis.Z ? direction.getOffsetZ() * g : h;
            world.addParticle(ParticleTypes.SMOKE, xOffset + i, yOffset + j, zOffset + k, 0.0, 0.0, 0.0);
            world.addParticle(ParticleTypes.FLAME, xOffset + i, yOffset + j, zOffset + k, 0.0, 0.0, 0.0);
        }
    }
}