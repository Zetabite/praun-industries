package zetabite.praunindustries.machines.systems;

import dev.onyxstudios.cca.api.v3.component.ComponentV3;
import net.minecraft.block.Block;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.nbt.NbtInt;
import net.minecraft.nbt.NbtList;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.World;
import org.jetbrains.annotations.NotNull;
import zetabite.praunindustries.block.ModBlocks;
import zetabite.praunindustries.machines.block.CokeOvenBrick;
import zetabite.praunindustries.machines.block.CokeOvenController;
import zetabite.praunindustries.machines.block.CokeOvenPeripheral;
import zetabite.praunindustries.machines.block.entity.CokeOvenControllerEntity;
import zetabite.praunindustries.machines.block.entity.CokeOvenPeripheralEntity;

import javax.annotation.Nullable;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

import static zetabite.praunindustries.PraunIndustries.LOGGER;
import static zetabite.praunindustries.machines.systems.CokeOvenManager.*;

public class CokeOvenManagerInstance implements ComponentV3 {

    private final ServerWorld serverWorld;
    private final Map<BlockPos, Set<BlockPos>> controllerList = new HashMap<>();
    private final Map<BlockPos, BlockPos> partList = new HashMap<>();
    private Set<BlockPos> destructionScheduler = new HashSet<>();
    private Map<BlockPos, Direction> reconstructionScheduler = new HashMap<>();

    public CokeOvenManagerInstance(World world) {
        if (world instanceof ServerWorld) {
            this.serverWorld = (ServerWorld) world;
            COKE_OVEN_MANAGER.registerInstance(this);
        } else {
            this.serverWorld = null;
        }
    }

    public World getServerWorld() {
        return serverWorld;
    }

    public void destroyCokeOven(Set<BlockPos> cokeOvenBricks) {
        scheduleForDestruction(cokeOvenBricks);
    }

    public boolean isCokeOvenPart(BlockPos brickPos) {
        return partList.containsKey(brickPos);
    }

    public boolean isCokeOvenPeripheral(BlockPos brickPos) {
        return isCokeOvenPart(brickPos) && !isCokeOvenController(brickPos);
    }

    public boolean isCokeOvenController(BlockPos brickPos) {
        return controllerList.containsKey(brickPos);
    }

    private boolean isScheduledForDestruction(BlockPos brickPos) {
        return destructionScheduler.contains(brickPos);
    }

    private boolean isScheduledForReconstruction(BlockPos brickPos) {
        return reconstructionScheduler.containsKey(brickPos);
    }

    public void scheduleForDestruction(Set<BlockPos> cokeOvenBricks) {
        if (serverWorld == null) return;
        Block block;

        for (BlockPos brickPos : cokeOvenBricks) {
            if (!isScheduledForDestruction(brickPos)) {
                block = serverWorld.getBlockState(brickPos).getBlock();

                if (block instanceof CokeOvenController) {
                    addToDestructionScheduler(brickPos);
                } else if (block instanceof CokeOvenPeripheral) {
                    addToDestructionScheduler(brickPos);
                }
            }
        }
    }

    public void scheduleForDestruction(BlockPos brickPos) {
        if (serverWorld == null) return;
        Block block;

        if (!isCokeOvenPart(brickPos)) {
            addToDestructionScheduler(brickPos);
            return;
        }
        Set<BlockPos> positions = isCokeOvenController(brickPos) ? controllerList.get(brickPos) : controllerList.get(partList.get(brickPos));

        for (BlockPos pos : positions) {
            if (!isScheduledForDestruction(pos)) {
                block = serverWorld.getBlockState(pos).getBlock();

                if (block instanceof CokeOvenController) {
                    addToDestructionScheduler(pos);
                } else if (block instanceof CokeOvenPeripheral) {
                    addToDestructionScheduler(pos);
                }
            }
        }

    }

    public void scheduleForReconstruction(BlockPos brickPos) {
        if (serverWorld == null) return;
        scheduleForReconstruction(brickPos, serverWorld.getBlockState(brickPos).get(CokeOvenPeripheral.FACING));
    }

    public void scheduleForReconstruction(BlockPos brickPos, Direction facing) {
        if (serverWorld == null) return;
        if (!isScheduledForReconstruction(brickPos)) {
            reconstructionScheduler.put(brickPos.mutableCopy(), facing);
        }
    }

    // Do not use for scheduling calling, this is purely an util function for better visibility
    private void addToDestructionScheduler(BlockPos brickPos) {
        if (serverWorld == null) return;
        if (!isScheduledForDestruction(brickPos)) {
            destructionScheduler.add(brickPos.mutableCopy());
        }
    }

    private void addToControllers(BlockPos controllerPos, Set<BlockPos> cokeOvenBricks) {
        if (serverWorld == null) return;
        if (!isCokeOvenController(controllerPos)) {
            controllerList.put(controllerPos.mutableCopy(), new HashSet<>(cokeOvenBricks));
        }
    }

    private void addToParts(BlockPos controllerPos, BlockPos partPos) {
        if (serverWorld == null) return;
        if (!isCokeOvenPart(partPos)) {
            partList.put(partPos.mutableCopy(), controllerPos.mutableCopy());
        }
    }

    private void removeFromDestructionScheduler(BlockPos brickPos) {
        if (serverWorld == null) return;
        if (isScheduledForDestruction(brickPos)) {
            destructionScheduler.remove(brickPos);
        }
    }

    private void removeFromReconstructionScheduler(BlockPos brickPos) {
        if (serverWorld == null) return;
        if (isScheduledForReconstruction(brickPos)) {
            reconstructionScheduler.remove(brickPos);
        }
    }

    private void removeFromControllers(BlockPos controllerPos) {
        if (serverWorld == null) return;
        if (isCokeOvenController(controllerPos)) {
            controllerList.remove(controllerPos);
        }
    }

    private void removeFromParts(BlockPos brickPos) {
        if (serverWorld == null) return;
        if (isCokeOvenPart(brickPos)) {
            partList.remove(brickPos);
        }
    }

    @Nullable
    public CokeOvenControllerEntity getCokeOvenController(BlockPos brickPos) {
        if (serverWorld == null) return null;
        if (isCokeOvenPart(brickPos)) return (CokeOvenControllerEntity) serverWorld.getBlockEntity(partList.get(brickPos));
        return null;
    }

    public void findAndAttemptToCreateCokeOven(BlockPos pos, Direction facing) {
        if (serverWorld == null) return;
        int x = pos.getX();
        int y = pos.getY();
        int z = pos.getZ();
        BlockPos.Mutable controllerPos = new BlockPos.Mutable();
        Set<BlockPos> posBlacklist = new HashSet<>();

        for (int xOffset : SEARCH_OFFSET) {
            for (int yOffset : SEARCH_OFFSET) {
                for (int zOffset : SEARCH_OFFSET) {
                    controllerPos.set(x + xOffset, y + yOffset, z + zOffset);

                    if (attemptToCreateCokeOven(controllerPos.mutableCopy(), facing, posBlacklist)) return;
                }
            }
        }
    }

    public boolean attemptToCreateCokeOven(BlockPos controllerPos, Direction facing, Set<BlockPos> posBlacklist) {
        if (serverWorld == null) return false;
        Set<BlockPos> cokeOvenBricks;

        if (serverWorld.getBlockState(controllerPos).getBlock() instanceof CokeOvenBrick) {
            cokeOvenBricks = getCokeOvenBricks(serverWorld, controllerPos, posBlacklist, StateFlag.CONSTRUCTION);

            // Try to create coke oven
            if (cokeOvenBricks.size() == 27) {
                addToControllers(controllerPos, cokeOvenBricks);

                int centerPosX = controllerPos.getX();
                int centerPosY = controllerPos.getY();
                int centerPosZ = controllerPos.getZ();

                // Give interface textures
                // TODO: Refactor to only change one block, since every other interface is not visible
                BlockPos.Mutable brickPos = new BlockPos.Mutable();
                Set<BlockPos> interfaces = new HashSet<>();

                for (int i = 0; i < 4; i++) {
                    brickPos.set(centerPosX + INTERFACE_OFFSETS[i][0], centerPosY, centerPosZ + INTERFACE_OFFSETS[i][1]);
                    CokeOvenBrick.replace(
                            serverWorld.getBlockState(brickPos),
                            ModBlocks.COKE_OVEN_PERIPHERAL.getDefaultState().with(CokeOvenPeripheral.INTERFACE, true).with(CokeOvenPeripheral.FACING, INTERFACE_FACING[i]),
                            serverWorld,
                            brickPos,
                            Block.NOTIFY_ALL
                    );
                    interfaces.add(brickPos.mutableCopy());
                }
                Iterator<BlockPos> cokeOvenBrick = cokeOvenBricks.iterator();
                boolean expectedValue;

                while (cokeOvenBrick.hasNext()){
                    brickPos = cokeOvenBrick.next().mutableCopy();

                    if (!interfaces.contains(brickPos)) {
                        if (brickPos.equals(controllerPos)) {
                            CokeOvenBrick.replace(
                                    serverWorld.getBlockState(brickPos),
                                    ModBlocks.COKE_OVEN_CONTROLLER.getDefaultState().with(CokeOvenController.FACING, facing),
                                    serverWorld,
                                    brickPos,
                                    Block.NOTIFY_ALL
                            );
                        } else {
                            CokeOvenBrick.replace(
                                    serverWorld.getBlockState(brickPos),
                                    ModBlocks.COKE_OVEN_PERIPHERAL.getDefaultState().with(CokeOvenPeripheral.FACING, facing),
                                    serverWorld,
                                    brickPos,
                                    Block.NOTIFY_ALL
                            );
                        }
                    }

                    if (brickPos.equals(controllerPos)) {
                        expectedValue = (serverWorld.getBlockEntity(brickPos) instanceof CokeOvenControllerEntity);
                    } else {
                        expectedValue = (serverWorld.getBlockEntity(brickPos) instanceof CokeOvenPeripheralEntity);
                    }

                    if (expectedValue) {
                        addToParts(controllerPos.mutableCopy(), brickPos.mutableCopy());
                    } else {
                        destroyCokeOven(cokeOvenBricks);
                        return false;
                    }
                    removeFromDestructionScheduler(brickPos);
                    removeFromReconstructionScheduler(brickPos);
                }
                LOGGER.info("Created coke oven at " + controllerPos);
                return true;
            }
        } else {
            if (!posBlacklist.contains(controllerPos)) posBlacklist.add(controllerPos.mutableCopy());
        }
        return false;
    }

    public void attemptToRecreateCokeOven(BlockPos controllerPos, Direction facing) {
        attemptToRecreateCokeOven(controllerPos, facing, new HashSet<>());
    }

    public void attemptToRecreateCokeOven(BlockPos controllerPos, Direction facing, Set<BlockPos> posBlacklist) {
        if (serverWorld == null) return;
        if (serverWorld.getBlockEntity(controllerPos) instanceof CokeOvenControllerEntity) {
            Set<BlockPos> cokeOvenBricks = getCokeOvenBricks(serverWorld, controllerPos, posBlacklist, StateFlag.RECONSTRUCTION);

            // Try to create coke oven
            if (cokeOvenBricks.size() == 27) {
                addToControllers(controllerPos.mutableCopy(), new HashSet<>(cokeOvenBricks));

                int centerPosX = controllerPos.getX();
                int centerPosY = controllerPos.getY();
                int centerPosZ = controllerPos.getZ();

                // Give interface textures
                // TODO: Refactor to only change one block, since every other interface is not visible
                BlockPos.Mutable brickPos = new BlockPos.Mutable();
                Set<BlockPos> interfaces = new HashSet<>();

                for (int i = 0; i < 4; i++) {
                    brickPos.set(centerPosX + INTERFACE_OFFSETS[i][0], centerPosY, centerPosZ + INTERFACE_OFFSETS[i][1]);
                    CokeOvenBrick.replace(
                            serverWorld.getBlockState(brickPos),
                            ModBlocks.COKE_OVEN_PERIPHERAL.getDefaultState().with(CokeOvenPeripheral.INTERFACE, true).with(CokeOvenPeripheral.FACING, INTERFACE_FACING[i]),
                            serverWorld,
                            brickPos,
                            Block.NOTIFY_ALL
                    );
                    interfaces.add(brickPos.mutableCopy());
                }
                Iterator<BlockPos> cokeOvenBrick = cokeOvenBricks.iterator();
                boolean expectedValue;

                while (cokeOvenBrick.hasNext()){
                    brickPos = cokeOvenBrick.next().mutableCopy();

                    if (!interfaces.contains(brickPos)) {
                        if (brickPos.equals(controllerPos)) {
                            serverWorld.setBlockState(brickPos, ModBlocks.COKE_OVEN_CONTROLLER.getDefaultState().with(CokeOvenController.FACING, facing), Block.NOTIFY_ALL);
                        } else {
                            serverWorld.setBlockState(brickPos, ModBlocks.COKE_OVEN_PERIPHERAL.getDefaultState().with(CokeOvenPeripheral.FACING, facing), Block.NOTIFY_ALL);
                        }
                    }

                    if (brickPos.equals(controllerPos)) {
                        expectedValue = (serverWorld.getBlockEntity(brickPos) instanceof CokeOvenControllerEntity);
                    } else {
                        expectedValue = (serverWorld.getBlockEntity(brickPos) instanceof CokeOvenPeripheralEntity);
                    }

                    if (expectedValue) {
                        addToParts(controllerPos.mutableCopy(), brickPos.mutableCopy());
                    } else {
                        destroyCokeOven(cokeOvenBricks);
                        return;
                    }
                    removeFromDestructionScheduler(brickPos);
                    removeFromReconstructionScheduler(brickPos);
                }
                LOGGER.info("Recreated coke oven at " + controllerPos);
            }
        } else {
            if (!posBlacklist.contains(controllerPos)) posBlacklist.add(controllerPos.mutableCopy());
        }
    }

    // Yeah, uh, you know, i dont think i am going to do something in case it crashes during a run of this, there is probably more fucked anyway.
    // If you are wondering what i am referring to, well, look a the reassignment of the schedulers, yeah, nasty stuff
    // Also hi people reading this comment, feel hugged, or not if you dont like being hugged
    public void tick() {
        if (serverWorld == null) return;

        AtomicReference<Map<BlockPos, Direction>> atomicReconstructionScheduler = new AtomicReference<>(reconstructionScheduler);

        if (!atomicReconstructionScheduler.get().isEmpty()) {
            for (BlockPos brickPos : atomicReconstructionScheduler.get().keySet()) {
                AtomicReference<BlockPos> atomicPos = new AtomicReference<>(brickPos.mutableCopy());
                attemptToRecreateCokeOven(atomicPos.get(), atomicReconstructionScheduler.get().get(atomicPos.get()));
            }
            reconstructionScheduler = new HashMap<>();
        }
        AtomicReference<Set<BlockPos>> atomicDestructionScheduler = new AtomicReference<>(destructionScheduler);

        if (!atomicDestructionScheduler.get().isEmpty()) {
            for (BlockPos brickPos : atomicDestructionScheduler.get()) {
                AtomicReference<BlockPos> atomicPos = new AtomicReference<>(brickPos.mutableCopy());

                if (serverWorld.getBlockEntity(atomicPos.get()) instanceof CokeOvenControllerEntity) {
                    CokeOvenController.replace(
                            serverWorld.getBlockState(atomicPos.get()),
                            ModBlocks.COKE_OVEN_BRICK.getDefaultState(),
                            serverWorld,
                            atomicPos.get(),
                            Block.NOTIFY_ALL
                    );
                } else if (serverWorld.getBlockEntity(atomicPos.get()) instanceof CokeOvenPeripheralEntity) {
                    CokeOvenPeripheral.replace(
                            serverWorld.getBlockState(atomicPos.get()),
                            ModBlocks.COKE_OVEN_BRICK.getDefaultState(),
                            serverWorld,
                            atomicPos.get(),
                            Block.NOTIFY_ALL
                    );
                }
                removeFromControllers(atomicPos.get());
                removeFromParts(atomicPos.get());
                destructionScheduler = new HashSet<>();
            }
        }
    }

    @Override
    public void readFromNbt(@NotNull NbtCompound tag) {
        if (serverWorld == null) return;
        LOGGER.info("Loading coke oven manager data for world " + serverWorld.getDimension().getSkyProperties().getPath());
        NbtList xList;
        NbtList yList;
        NbtList zList;
        BlockPos.Mutable brickPos = new BlockPos.Mutable();

        LOGGER.info("Reading destruction scheduler from disk");

        if (tag.contains("destructionSchedulerTag")) {
            NbtCompound destructionSchedulerTag = tag.getCompound("destructionSchedulerTag");
            xList = destructionSchedulerTag.getList("x", NbtInt.INT_TYPE);
            yList = destructionSchedulerTag.getList("y", NbtInt.INT_TYPE);
            zList = destructionSchedulerTag.getList("z", NbtInt.INT_TYPE);

            if ((xList.size() + yList.size() + zList.size()) % 3 == 0) {
                for (int i = 0; i < xList.size(); i++) {
                    brickPos.set(xList.getInt(i), yList.getInt(i), zList.getInt(i));
                    scheduleForDestruction(brickPos);
                }
            }
        }
        LOGGER.info("Done reading destruction scheduler from disk");

        LOGGER.info("Reading controller positions from disk");

        if (tag.contains("controllersTag")) {
            NbtCompound controllersTag = tag.getCompound("controllersTag");

            xList = controllersTag.getList("x", NbtInt.INT_TYPE);
            yList = controllersTag.getList("y", NbtInt.INT_TYPE);
            zList = controllersTag.getList("z", NbtInt.INT_TYPE);

            if ((xList.size() + yList.size() + zList.size()) % 3 == 0) {
                for (int i = 0; i < xList.size(); i++) {
                    brickPos.set(xList.getInt(i), yList.getInt(i), zList.getInt(i));
                    scheduleForReconstruction(brickPos);
                }
            }
        }
        LOGGER.info("Done reading controller positions from disk");
    }

    @Override
    public void writeToNbt(@NotNull NbtCompound tag) {
        if (serverWorld == null) return;
        LOGGER.info("Saving coke oven manager data for world " + serverWorld.getDimension().getSkyProperties().getPath());
        NbtList xList;
        NbtList yList;
        NbtList zList;

        // Writing Controllers to disk
        LOGGER.info("Writing controller positions to disk");
        NbtCompound controllersTag = new NbtCompound();
        xList = new NbtList();
        yList = new NbtList();
        zList = new NbtList();

        for (BlockPos controllerPos : controllerList.keySet()) {
            xList.add(NbtInt.of(controllerPos.getX()));
            yList.add(NbtInt.of(controllerPos.getY()));
            zList.add(NbtInt.of(controllerPos.getZ()));
        }
        controllersTag.put("x", xList);
        controllersTag.put("y", yList);
        controllersTag.put("z", zList);
        tag.put("controllersTag", controllersTag);
        LOGGER.info("Done writing controller positions to disk");

        // Writing Destruction Scheduler to disk
        LOGGER.info("Writing destruction scheduler to disk");
        NbtCompound destructionSchedulerTag = new NbtCompound();
        xList = new NbtList();
        yList = new NbtList();
        zList = new NbtList();

        for (BlockPos brickPos : destructionScheduler) {
            xList.add(NbtInt.of(brickPos.getX()));
            yList.add(NbtInt.of(brickPos.getY()));
            zList.add(NbtInt.of(brickPos.getZ()));
        }
        destructionSchedulerTag.put("x", xList);
        destructionSchedulerTag.put("y", yList);
        destructionSchedulerTag.put("z", zList);
        tag.put("destructionSchedulerTag", destructionSchedulerTag);
        LOGGER.info("Done writing destruction positions to disk");
    }
}
