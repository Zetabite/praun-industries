package zetabite.praunindustries.machines.systems;

import net.minecraft.block.Block;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.util.registry.RegistryKey;
import net.minecraft.world.World;
import zetabite.praunindustries.machines.block.CokeOvenBrick;
import zetabite.praunindustries.machines.block.CokeOvenController;
import zetabite.praunindustries.machines.block.CokeOvenPeripheral;
import zetabite.praunindustries.machines.block.entity.CokeOvenControllerEntity;

import javax.annotation.Nullable;
import java.util.*;

public class CokeOvenManager {

    public static CokeOvenManager COKE_OVEN_MANAGER = new CokeOvenManager();
    private final Set<CokeOvenManagerInstance> instances = new HashSet<>();
    private final Map<RegistryKey<World>, CokeOvenManagerInstance> instanceDictionary = new HashMap<>();
    public static final int[][] INTERFACE_OFFSETS = new int[][] {{1, 0}, {-1, 0}, {0, 1}, {0, -1}};
    public static final Direction[] INTERFACE_FACING = new Direction[] {Direction.EAST, Direction.WEST, Direction.SOUTH, Direction.NORTH};
    public static final int[] SEARCH_OFFSET = new int[]{-2, -1, 0, 1, 2};
    public static final int[] CREATION_OFFSET = new int[]{-1, 0, 1};
    public enum StateFlag {
        CONSTRUCTION(0),
        RECONSTRUCTION(1),
        DESTRUCTION(2);

        private final int value;

        StateFlag(int value) {
            this.value = value;
        }

        public int get() {
            return value;
        }
    }

    public static Set<BlockPos> getCokeOvenBricks(World world, BlockPos controllerPos, Set<BlockPos> posBlacklist, StateFlag stateFlag) {
        if (world instanceof ServerWorld) {
            Set<BlockPos> cokeOvenBricks = new HashSet<>();
            BlockPos.Mutable brickPos = new BlockPos.Mutable();

            int x = controllerPos.getX();
            int y = controllerPos.getY();
            int z = controllerPos.getZ();

            for (int xOffset : CREATION_OFFSET) {
                for (int yOffset : CREATION_OFFSET) {
                    for (int zOffset : CREATION_OFFSET) {
                        brickPos.set(x + xOffset, y + yOffset, z + zOffset);

                        if (posBlacklist.contains(brickPos)) {
                            return cokeOvenBricks;
                        }
                        Block block = world.getBlockState(brickPos).getBlock();

                        if (StateFlag.DESTRUCTION != stateFlag && block instanceof CokeOvenBrick) {
                            cokeOvenBricks.add(brickPos.mutableCopy());
                        } else if (StateFlag.RECONSTRUCTION == stateFlag && block instanceof CokeOvenPeripheral) {
                            cokeOvenBricks.add(brickPos.mutableCopy());
                        } else if (StateFlag.RECONSTRUCTION == stateFlag && block instanceof CokeOvenController) {
                            cokeOvenBricks.add(brickPos.mutableCopy());
                        } else {
                            posBlacklist.add(brickPos.mutableCopy());
                            return cokeOvenBricks;
                        }
                    }
                }
            }
            return cokeOvenBricks;
        }
        return new HashSet<>();
    }

    @Nullable
    public CokeOvenControllerEntity getCokeOvenController(World world, BlockPos brickPos) {
        if (!(world instanceof ServerWorld)) return null;
        return getInstanceForWorld(world).getCokeOvenController(brickPos);
    }

    public void registerInstance(CokeOvenManagerInstance instance) {
        if (instances.contains(instance)) return;
        if (!(instance.getServerWorld() instanceof ServerWorld)) return;
        instances.add(instance);
        instanceDictionary.put(instance.getServerWorld().getRegistryKey(), instance);
    }

    private CokeOvenManagerInstance getInstanceForWorld(World world) {
        if (!(world instanceof ServerWorld)) return null;
        RegistryKey<World> worldRegistryKey = world.getRegistryKey();

        if (instanceDictionary.containsKey(worldRegistryKey)) {
            return instanceDictionary.get(worldRegistryKey);
        }
        return null;
    }

    public void scheduleForDestruction(World world, BlockPos brickPos) {
        CokeOvenManagerInstance instance = getInstanceForWorld(world);

        if (instance != null) instance.scheduleForDestruction(brickPos);
    }

    public void findAndAttemptToCreateCokeOven(World world, BlockPos brickPos, Direction facing) {
        CokeOvenManagerInstance instance = getInstanceForWorld(world);

        if (instance != null) instance.findAndAttemptToCreateCokeOven(brickPos, facing);
    }

    public void tick(World world) {
        CokeOvenManagerInstance instance = getInstanceForWorld(world);

        if (instance != null) instance.tick();
    }
}
