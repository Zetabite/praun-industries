package zetabite.praunindustries.machines.screen;

// Minecraft / Mojang
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventory;
import net.minecraft.inventory.SimpleInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.recipe.*;
import net.minecraft.recipe.book.RecipeBookCategory;
import net.minecraft.screen.*;

// Praun Industries
import net.minecraft.screen.slot.Slot;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.world.World;
import zetabite.praunindustries.machines.block.entity.CokeOvenControllerEntity;
import zetabite.praunindustries.machines.recipe.CokeOvenRecipe;
import zetabite.praunindustries.recipe.ModRecipes;
import zetabite.praunindustries.screen.ModScreenHandlers;

import static zetabite.praunindustries.machines.block.entity.CokeOvenControllerEntity.*;

public class CokeOvenScreenHandler extends AbstractRecipeScreenHandler<Inventory> {

    public static final int INPUT_SLOT = 0;
    public static final int OUTPUT_SLOT = 1;
    public static final int SLOTS_COUNT = 2;
    private static final int INVENTORY_SLOTS_START = SLOTS_COUNT;
    private static final int INVENTORY_SLOTS_END = INVENTORY_SLOTS_START + 27;
    private static final int HOTBAR_SLOTS_START = INVENTORY_SLOTS_END;
    private static final int HOTBAR_SLOTS_END = HOTBAR_SLOTS_START + 9;
    private final Inventory inventory;
    // TODO: Figure out what this does
    private final PropertyDelegate propertyDelegate;
    protected final World world;

    public CokeOvenScreenHandler(int i, PlayerInventory playerInventory) {
        this(i, playerInventory, new SimpleInventory(SLOTS_COUNT), new ArrayPropertyDelegate(DATA_VALUES));
    }

    public CokeOvenScreenHandler(int i, PlayerInventory playerInventory, Inventory inventory, PropertyDelegate propertyDelegate) {
        super(ModScreenHandlers.COKE_OVEN, i);
        checkSize(inventory, SLOTS_COUNT);
        checkDataCount(propertyDelegate, DATA_VALUES);
        this.inventory = inventory;
        this.propertyDelegate = propertyDelegate;
        this.world = playerInventory.player.world;
        // Slot, x, y
        // (1, 56, 53)
        this.addSlot(new CokeOvenInputSlot(this, inventory, INPUT_SLOT, 46, 36));
        // Slot, x, y
        // (2, 116, 35)
        this.addSlot(new CokeOvenOutputSlot(playerInventory.player, inventory, OUTPUT_SLOT, 116, 35));

        // Build Player Non Hotbar Inventory GUI for Coke Oven GUI
        for(int j = 0; j < 3; ++j) {
            for(int k = 0; k < 9; ++k) {
                this.addSlot(new Slot(playerInventory, k + j * 9 + 9, 8 + k * 18, 84 + j * 18));
            }
        }

        // Build Player Hotbar Inventory GUI for Coke Oven GUI
        for(int j = 0; j < 9; ++j) {
            this.addSlot(new Slot(playerInventory, j, 8 + j * 18, 142));
        }

        this.addProperties(propertyDelegate);
    }

    // Guessing this adds the recipe to the book or looks up the recipe in registry
    @Override
    public void populateRecipeFinder(RecipeMatcher finder) {
        if (inventory instanceof RecipeInputProvider recipeInputProvider) {
            recipeInputProvider.provideRecipeInputs(finder);
        }
    }

    @Override
    public void clearCraftingSlots() {
        // TODO: Revisit what this does
        this.getSlot(INPUT_SLOT).setStack(ItemStack.EMPTY);
        this.getSlot(OUTPUT_SLOT).setStack(ItemStack.EMPTY);
    }

    @Override
    public boolean matches(Recipe<? super Inventory> recipe) {
        return recipe.matches(inventory, world);
    }

    @Override
    public int getCraftingResultSlotIndex() {
        return OUTPUT_SLOT;
    }

    // If i am correct, this should be 3 x 3 for Crafting Table, and for every other furnace 1 x 1
    @Override
    public int getCraftingWidth() {
        return 1;
    }

    @Override
    public int getCraftingHeight() {
        return 1;
    }

    @Override
    public int getCraftingSlotCount() {
        return SLOTS_COUNT;
    }

    @Override
    public RecipeBookCategory getCategory() {
        return RecipeBookCategory.FURNACE;
    }

    @Override
    public boolean canInsertIntoSlot(int index) {
        return index != OUTPUT_SLOT;
    }

    @Override
    public boolean canUse(PlayerEntity player) {
        return inventory.canPlayerUse(player);
    }

    public boolean isBurning() {
        return propertyDelegate.get(IS_BURNING_DATA) > 0;
    }

    public int getCookProgress() {
        int cookingTime = propertyDelegate.get(COOK_TIME_DATA);
        int totalCookingTime = propertyDelegate.get(TOTAL_COOK_TIME_DATA);

        float calculationResult = 14.0F;
        calculationResult /= totalCookingTime;
        calculationResult *= cookingTime;
        return (int) Math.ceil(calculationResult);
    }

    public boolean hasCreosote() {
        int creosoteLevel = propertyDelegate.get(CREOSOTE_LEVEL_DATA);
        return creosoteLevel > 0;
    }
    public int getCreosoteLevel() {
        int creosoteLevel = propertyDelegate.get(CREOSOTE_LEVEL_DATA);

        float calculationResult = 54.0F;
        calculationResult /= MAX_CREOSOTE_LEVEL;
        calculationResult *= creosoteLevel;
        return (int) Math.ceil(calculationResult);
    }

    protected boolean isCookable(ItemStack itemStack) {
        return world.getRecipeManager().getFirstMatch(ModRecipes.COKE_OVEN_RECIPE_TYPE, inventory, world).map(CokeOvenRecipe::getCookTime).orElse(STANDARD_PRODUCTION_TIME) > 0;
    }

    // TODO: More commenting for future understanding
    @Override
    public ItemStack transferSlot(PlayerEntity player, int index) {
        ItemStack tempStack = ItemStack.EMPTY;
        Slot slot = slots.get(index);

        if (slot.hasStack()) {
            ItemStack stackOfSlot = slot.getStack();
            tempStack = stackOfSlot.copy();

            if (index == OUTPUT_SLOT) {
                if (!insertItem(stackOfSlot, INVENTORY_SLOTS_START, HOTBAR_SLOTS_END, true)) {
                    return ItemStack.EMPTY;
                }
                slot.onQuickTransfer(stackOfSlot, tempStack);
            } else if (index != INPUT_SLOT) {
                if (isCookable(stackOfSlot)) {
                    if (!insertItem(stackOfSlot, INPUT_SLOT, OUTPUT_SLOT, false)) {
                        return ItemStack.EMPTY;
                    }
                } else if (index >= INVENTORY_SLOTS_START && index < INVENTORY_SLOTS_END) {
                    if (!insertItem(stackOfSlot, HOTBAR_SLOTS_START, HOTBAR_SLOTS_END, false)) {
                        return ItemStack.EMPTY;
                    }
                } else if (index >= HOTBAR_SLOTS_START && index < HOTBAR_SLOTS_END && !insertItem(stackOfSlot, INVENTORY_SLOTS_START, INVENTORY_SLOTS_END, false)) {
                    return ItemStack.EMPTY;
                }
            } else if (!insertItem(stackOfSlot, INVENTORY_SLOTS_START, HOTBAR_SLOTS_END, false)) {
                return ItemStack.EMPTY;
            }

            if (stackOfSlot.isEmpty()) {
                slot.setStack(ItemStack.EMPTY);
            } else {
                slot.markDirty();
            }

            if (stackOfSlot.getCount() == tempStack.getCount()) {
                return ItemStack.EMPTY;
            }
            slot.onTakeItem(player, stackOfSlot);
        }

        return tempStack;
    }

    public class CokeOvenInputSlot extends Slot {

        private final CokeOvenScreenHandler handler;

        public CokeOvenInputSlot(CokeOvenScreenHandler cokeOvenScreenHandler, Inventory inventory, int slot, int x, int y) {
            super(inventory, slot, x, y);
            this.handler = cokeOvenScreenHandler;
        }

        @Override
        public boolean canInsert(ItemStack stack) {
            return isCookable(stack);
        }

        @Override
        public int getMaxItemCount(ItemStack stack) {
            return super.getMaxItemCount(stack);
        }
    }

    public class CokeOvenOutputSlot extends Slot {
        private final PlayerEntity player;
        private int amount;

        public CokeOvenOutputSlot(PlayerEntity playerEntity, Inventory inventory, int slot, int x, int y) {
            super(inventory, slot, x, y);
            this.player = playerEntity;
        }

        @Override
        public boolean canInsert(ItemStack stack) {
            return false;
        }

        @Override
        public ItemStack takeStack(int amount) {
            if (this.hasStack()) {
                this.amount += Math.min(amount, this.getStack().getCount());
            }

            return super.takeStack(amount);
        }

        @Override
        public void onTakeItem(PlayerEntity player, ItemStack stack) {
            onCrafted(stack);
            super.onTakeItem(player, stack);
        }

        @Override
        protected void onCrafted(ItemStack stack, int amount) {
            this.amount += amount;
            onCrafted(stack);
        }

        @Override
        protected void onCrafted(ItemStack stack) {
            stack.onCraft(player.world, player, amount);

            if (player instanceof ServerPlayerEntity serverPlayerEntity) {
                if (inventory instanceof CokeOvenControllerEntity cokeOvenPartEntity) {
                    cokeOvenPartEntity.unlockRecipes(serverPlayerEntity);
                }
            }
            amount = 0;
        }
    }
}
