package zetabite.praunindustries.machines.recipe;

import net.minecraft.inventory.Inventory;
import net.minecraft.item.ItemStack;
import net.minecraft.recipe.*;
import net.minecraft.util.Identifier;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.world.World;
import zetabite.praunindustries.item.ModItems;
import zetabite.praunindustries.recipe.ModRecipes;

public class CokeOvenRecipe implements Recipe<Inventory> {

    protected final RecipeType<?> type = ModRecipes.COKE_OVEN_RECIPE_TYPE;
    protected final Identifier id;
    protected final String group;
    protected final Ingredient input;
    protected final ItemStack output;
    protected final int cookTime;
    protected final long creosoteAdded;

    public CokeOvenRecipe(Identifier identifier, String string, Ingredient ingredient, ItemStack itemStack, int cookTime, long creosoteAdded) {
        this.id = identifier;
        this.group = string;
        this.input = ingredient;
        this.output = itemStack;
        this.cookTime = cookTime;
        this.creosoteAdded = creosoteAdded;
    }

    @Override
    public ItemStack createIcon() {
        return new ItemStack(ModItems.COKE_OVEN_PERIPHERAL);
    }

    @Override
    public RecipeSerializer<?> getSerializer() {
        return ModRecipes.COKE_OVEN_RECIPE_SERIALIZER;
    }

    @Override
    public boolean matches(Inventory inventory, World world) {
        return input.test(inventory.getStack(0));
    }

    @Override
    public ItemStack craft(Inventory inventory) {
        return output.copy();
    }

    @Override
    public boolean fits(int width, int height) {
        return true;
    }

    @Override
    public DefaultedList<Ingredient> getIngredients() {
        DefaultedList<Ingredient> defaultedList = DefaultedList.of();
        defaultedList.add(input);
        return defaultedList;
    }

    @Override
    public ItemStack getOutput() {
        return this.output;
    }

    @Override
    public String getGroup() {
        return group;
    }

    public int getCookTime() {
        return cookTime;
    }

    public long getCreosoteAdded() {
        return creosoteAdded;
    }

    @Override
    public Identifier getId() {
        return id;
    }

    @Override
    public RecipeType<?> getType() {
        return type;
    }
}
