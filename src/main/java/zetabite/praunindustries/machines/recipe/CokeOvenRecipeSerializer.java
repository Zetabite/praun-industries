package zetabite.praunindustries.machines.recipe;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.recipe.Ingredient;
import net.minecraft.util.Identifier;
import net.minecraft.util.JsonHelper;
import net.minecraft.util.registry.Registry;
import org.quiltmc.qsl.recipe.api.serializer.QuiltRecipeSerializer;

public class CokeOvenRecipeSerializer <T extends CokeOvenRecipe> implements QuiltRecipeSerializer<T> {

    private final int cookingTime;
    private final long creosoteAdded;
    public final CokeOvenRecipeSerializer.RecipeFactory<T> recipeFactory;

    public CokeOvenRecipeSerializer(CokeOvenRecipeSerializer.RecipeFactory<T> recipeFactory, int cookingTime, long creosoteAdded) {
        this.cookingTime = cookingTime;
        this.creosoteAdded = creosoteAdded;
        this.recipeFactory = recipeFactory;
    }

    public T read(Identifier identifier, JsonObject jsonObject) {
        String string = JsonHelper.getString(jsonObject, "group", "");
        JsonElement jsonElement = (JsonHelper.hasArray(jsonObject, "ingredient")
                ? JsonHelper.getArray(jsonObject, "ingredient")
                : JsonHelper.getObject(jsonObject, "ingredient"));
        Ingredient ingredient = Ingredient.fromJson(jsonElement);
        String string2 = JsonHelper.getString(jsonObject, "result");
        Identifier identifier2 = new Identifier(string2);
        ItemStack itemStack = new ItemStack(
                Registry.ITEM.getOrEmpty(identifier2).orElseThrow(() -> new IllegalStateException("Item: " + string2 + " does not exist"))
        );
        int cookingTime = JsonHelper.getInt(jsonObject, "cookingtime", this.cookingTime);
        long creosoteAdded = JsonHelper.getLong(jsonObject, "creosote_added", this.creosoteAdded);
        return this.recipeFactory.create(identifier, string, ingredient, itemStack, cookingTime, creosoteAdded);
    }

    public T read(Identifier identifier, PacketByteBuf packetByteBuf) {
        String string = packetByteBuf.readString();
        Ingredient ingredient = Ingredient.fromPacket(packetByteBuf);
        ItemStack itemStack = packetByteBuf.readItemStack();
        int cookingTime = packetByteBuf.readVarInt();
        long creosoteAdded = packetByteBuf.readVarLong();
        return this.recipeFactory.create(identifier, string, ingredient, itemStack, cookingTime, creosoteAdded);
    }

    public void write(PacketByteBuf packetByteBuf, T cokeOvenRecipe) {
        packetByteBuf.writeString(cokeOvenRecipe.group);
        cokeOvenRecipe.input.write(packetByteBuf);
        packetByteBuf.writeItemStack(cokeOvenRecipe.output);
        packetByteBuf.writeVarInt(cokeOvenRecipe.cookTime);
        packetByteBuf.writeVarLong(cokeOvenRecipe.creosoteAdded);
    }

    @Override
    public JsonObject toJson(T recipe) {
        return null;
    }

    public interface RecipeFactory<T> {
        T create(Identifier identifier, String string, Ingredient ingredient, ItemStack itemStack, int cookingTime, long creosoteAdded);
    }
}
