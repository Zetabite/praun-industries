package zetabite.praunindustries.machines.client.gui.screen.ingame;

// Minecraft / Mojang
import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.gui.screen.ingame.HandledScreen;
import net.minecraft.client.render.GameRenderer;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.screen.slot.Slot;
import net.minecraft.screen.slot.SlotActionType;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;

// Fabric
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import zetabite.praunindustries.machines.screen.CokeOvenScreenHandler;

// Praun Industries

import static zetabite.praunindustries.PraunIndustries.MODID;

@Environment(EnvType.CLIENT)
public class CokeOvenScreen extends HandledScreen<CokeOvenScreenHandler> {

    private static final Identifier COKE_OVEN_GUI_TEXTURE = new Identifier(MODID, "textures/gui/container/coke_oven.png");
    private boolean narrow;

    public CokeOvenScreen(CokeOvenScreenHandler cokeOvenScreenHandler, PlayerInventory playerInventory, Text title) {
        super(cokeOvenScreenHandler, playerInventory, title);
    }

    @Override
    public void init() {
        super.init();
        narrow = width < 379;
        titleX = (backgroundWidth - textRenderer.getWidth(title)) / 2;
    }

    @Override
    public void handledScreenTick() {
        super.handledScreenTick();
    }

    @Override
    public void render(MatrixStack matrices, int mouseX, int mouseY, float delta) {
        this.renderBackground(matrices);

        if (narrow) {
            drawBackground(matrices, delta, mouseX, mouseY);
        } else {
            super.render(matrices, mouseX, mouseY, delta);
        }
        drawMouseoverTooltip(matrices, mouseX, mouseY);
    }

    @Override
    protected void drawBackground(MatrixStack matrices, float delta, int mouseX, int mouseY) {
        RenderSystem.setShader(GameRenderer::getPositionTexShader);
        RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
        RenderSystem.setShaderTexture(0, COKE_OVEN_GUI_TEXTURE);
        int i = x;
        int j = y;
        drawTexture(matrices, i, j, 0, 0, backgroundWidth, backgroundHeight);

        if (handler.isBurning()) {
            int cookingProgress = handler.getCookProgress();
            int burnTextureHeight = 14;
            drawTexture(
                    matrices,
                    i + 80,
                    j + 36 + burnTextureHeight - cookingProgress,
                    176,
                    burnTextureHeight - cookingProgress,
                    14,
                    cookingProgress
            );
        }

        if (handler.hasCreosote()) {
            int creosoteLevel = handler.getCreosoteLevel();
            int creosoteOverlayHeight = 54;
            drawTexture(
                    matrices,
                    i + 152,
                    j + 15 + creosoteOverlayHeight - creosoteLevel,
                    176,
                    14 + creosoteOverlayHeight - creosoteLevel,
                    14,
                    creosoteLevel
            );
        }

    }

    @Override
    public boolean mouseClicked(double mouseX, double mouseY, int button) {
        return narrow || super.mouseClicked(mouseX, mouseY, button);
    }

    @Override
    protected void onMouseClick(Slot slot, int slotId, int button, SlotActionType actionType) {
        super.onMouseClick(slot, slotId, button, actionType);
    }

    @Override
    public boolean keyPressed(int keyCode, int scanCode, int modifiers) {
        return super.keyPressed(keyCode, scanCode, modifiers);
    }

    @Override
    protected boolean isClickOutsideBounds(double mouseX, double mouseY, int left, int top, int button) {
        return mouseX < (double) left || mouseY < (double) top || mouseX >= (double) (left + backgroundWidth) || mouseY >= (double) (top + backgroundHeight);
    }

    @Override
    public boolean charTyped(char chr, int modifiers) {
        return super.charTyped(chr, modifiers);
    }

    @Override
    public void removed() {
        super.removed();
    }
}