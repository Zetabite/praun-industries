package zetabite.praunindustries.emi.recipe;

import dev.emi.emi.EmiPort;
import dev.emi.emi.EmiRenderHelper;
import dev.emi.emi.api.recipe.EmiRecipe;
import dev.emi.emi.api.recipe.EmiRecipeCategory;
import dev.emi.emi.api.render.EmiTexture;
import dev.emi.emi.api.stack.EmiIngredient;
import dev.emi.emi.api.stack.EmiStack;
import dev.emi.emi.api.widget.WidgetHolder;
import net.minecraft.client.font.TextRenderer;
import net.minecraft.client.gui.tooltip.TooltipComponent;
import net.minecraft.text.MutableText;
import net.minecraft.util.Identifier;
import zetabite.praunindustries.emi.PraunIndustriesEmiPlugin;
import zetabite.praunindustries.machines.recipe.CokeOvenRecipe;

import java.util.List;

public class EmiCokeOvenRecipe implements EmiRecipe {
    private final Identifier id;
    private final EmiIngredient input;
    private final EmiStack output;
    private final CokeOvenRecipe recipe;

    public EmiCokeOvenRecipe(CokeOvenRecipe recipe) {
        this.id = recipe.getId();
        input = EmiIngredient.of(recipe.getIngredients().get(0));
        output = EmiStack.of(recipe.getOutput());
        this.recipe = recipe;
    }

    @Override
    public EmiRecipeCategory getCategory() {
        return PraunIndustriesEmiPlugin.COKE_OVEN_CATEGORY;
    }

    @Override
    public Identifier getId() {
        return id;
    }

    @Override
    public List<EmiIngredient> getInputs() {
        return List.of(input);
    }

    @Override
    public List<EmiStack> getOutputs() {
        return List.of(output);
    }

    @Override
    public int getDisplayWidth() {
        return 82;
    }

    @Override
    public int getDisplayHeight() {
        return 48;
    }

    @Override
    public void addWidgets(WidgetHolder widgets) {
        TextRenderer textRenderer = EmiRenderHelper.CLIENT.textRenderer;
        MutableText amountText = EmiPort.translatable("praunindustries.coke_oven.creosote_added0", recipe.getCreosoteAdded());
        MutableText fluidName = EmiPort.translatable("praunindustries.coke_oven.creosote_added1");
        
        widgets.addTexture(EmiTexture.EMPTY_FLAME, 32, 5).tooltip((mx, my) -> List.of(TooltipComponent.of(EmiPort.ordered(EmiPort.translatable("emi.cooking.time", recipe.getCookTime() / 20f)))));
        widgets.addAnimatedTexture(EmiTexture.FULL_FLAME, 32, 5, 50 * recipe.getCookTime(), false, true, false);
        int width = getDisplayWidth();
        int textX = (width - textRenderer.getWidth(amountText)) / 2;
        widgets.addText(EmiPort.ordered(amountText), textX, 28, -1, true);
        textX = (width - textRenderer.getWidth(fluidName)) / 2;
        widgets.addText(EmiPort.ordered(fluidName), textX, 38, -1, true);
        widgets.addSlot(input, 0, 4);
        widgets.addSlot(output, 56, 0).output(true).recipeContext(this);
    }
}
