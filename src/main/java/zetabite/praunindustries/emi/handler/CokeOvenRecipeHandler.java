package zetabite.praunindustries.emi.handler;

import com.google.common.collect.Lists;
import dev.emi.emi.api.EmiRecipeHandler;
import dev.emi.emi.api.recipe.EmiRecipe;
import dev.emi.emi.api.recipe.EmiRecipeCategory;
import net.minecraft.screen.slot.Slot;
import zetabite.praunindustries.machines.screen.CokeOvenScreenHandler;

import java.util.List;

public class CokeOvenRecipeHandler<T extends CokeOvenScreenHandler> implements EmiRecipeHandler<T> {

    private final EmiRecipeCategory category;

    public CokeOvenRecipeHandler(EmiRecipeCategory category) {
        this.category = category;
    }

    @Override
    public List<Slot> getInputSources(T handler) {
        List<Slot> list = Lists.newArrayList();
        list.add(handler.getSlot(0));
        int invStart = 2;
        for (int i = invStart; i < invStart + 36; i++) {
            list.add(handler.getSlot(i));
        }
        return list;
    }

    @Override
    public List<Slot> getCraftingSlots(T handler) {
        return List.of(handler.slots.get(0));
    }

    @Override
    public boolean supportsRecipe(EmiRecipe recipe) {
        return recipe.getCategory() == category && recipe.supportsRecipeTree();
    }
}
