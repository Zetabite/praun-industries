package zetabite.praunindustries.emi;

import com.mojang.blaze3d.systems.RenderSystem;
import dev.emi.emi.EmiRenderHelper;
import dev.emi.emi.api.EmiPlugin;
import dev.emi.emi.api.EmiRegistry;
import dev.emi.emi.api.recipe.EmiRecipeCategory;
import dev.emi.emi.api.recipe.EmiRecipeSorting;
import dev.emi.emi.api.render.EmiRenderable;
import dev.emi.emi.api.stack.EmiStack;
import net.minecraft.client.gui.DrawableHelper;
import net.minecraft.recipe.RecipeManager;
import net.minecraft.util.Identifier;
import zetabite.praunindustries.emi.recipe.EmiCokeOvenRecipe;
import zetabite.praunindustries.item.ModItems;
import zetabite.praunindustries.machines.recipe.CokeOvenRecipe;
import zetabite.praunindustries.recipe.ModRecipes;
import zetabite.praunindustries.util.ModContentID;

import static zetabite.praunindustries.PraunIndustries.MODID;

public class PraunIndustriesEmiPlugin implements EmiPlugin {

    public static final EmiStack COKE_OVEN = EmiStack.of(ModItems.COKE_OVEN_BRICK);
    public static final EmiRecipeCategory COKE_OVEN_CATEGORY = new EmiRecipeCategory(
            new Identifier(MODID, ModContentID.COKE_OVEN),
            COKE_OVEN,
            (matrices, x, y, delta) -> {
                RenderSystem.setShaderTexture(0, EmiRenderHelper.WIDGETS);
                DrawableHelper.drawTexture(matrices, x, y, 240, 240, 16, 16, 256, 256);
            },
            EmiRecipeSorting.compareOutputThenInput()
    );

    @Override
    public void register(EmiRegistry registry) {
        registry.addCategory(COKE_OVEN_CATEGORY);
        registry.addWorkstation(COKE_OVEN_CATEGORY, COKE_OVEN);
        RecipeManager manager = registry.getRecipeManager();

        for (CokeOvenRecipe recipe : manager.listAllOfType(ModRecipes.COKE_OVEN_RECIPE_TYPE)) {
            registry.addRecipe(new EmiCokeOvenRecipe(recipe));
        }
    }
}
