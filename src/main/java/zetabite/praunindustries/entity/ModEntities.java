package zetabite.praunindustries.entity;

// Minecraft / Mojang
import net.fabricmc.fabric.api.client.rendering.v1.EntityModelLayerRegistry;
import net.minecraft.client.render.entity.model.BeeEntityModel;
import net.minecraft.client.render.entity.model.EntityModelLayer;
import net.minecraft.entity.EntityDimensions;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.SpawnGroup;
import net.minecraft.entity.passive.BeeEntity;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

// Fabric
import net.fabricmc.fabric.api.object.builder.v1.entity.FabricEntityTypeBuilder;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.rendering.v1.EntityRendererRegistry;
import net.fabricmc.fabric.api.object.builder.v1.entity.FabricDefaultAttributeRegistry;

// Praun Industries
import zetabite.praunindustries.util.ModContentID;
import zetabite.praunindustries.PraunIndustries;
import zetabite.praunindustries.bees.client.render.entity.ResourceBeeEntityRenderer;
import zetabite.praunindustries.bees.entity.passive.ResourceBeeEntity;
import zetabite.praunindustries.util.ContentRegistryUtil;

public class ModEntities extends ContentRegistryUtil {
    public static final EntityType<ResourceBeeEntity> RESOURCE_BEE;
    public static final EntityModelLayer MODEL_RESOURCE_BEE_LAYER = new EntityModelLayer(new Identifier(PraunIndustries.MODID, "resource_bee"), "main");

    static {
        RESOURCE_BEE = Registry.register(
                Registry.ENTITY_TYPE,
                new Identifier(PraunIndustries.MODID, ModContentID.RESOURCE_BEE),
                FabricEntityTypeBuilder.create(SpawnGroup.CREATURE, ResourceBeeEntity::new).dimensions(EntityDimensions.fixed(0.7F, 0.6F)).trackRangeChunks(8).build()
        );
    }

    public static void onInitialize() {
        FabricDefaultAttributeRegistry.register(RESOURCE_BEE, BeeEntity.createBeeAttributes());
    }

    @Environment(EnvType.CLIENT)
    public static void onInitializeClient() {
        /*
        for (BeeType beeType : BeeTypeImpl.TYPES.inverse().keySet()) {
            EntityRendererRegistry.register(RESOURCE_BEE, (context) -> new ResourceBeeEntityRenderer(context, beeType));
        }
         */
        EntityRendererRegistry.register(RESOURCE_BEE, ResourceBeeEntityRenderer::new);
        EntityModelLayerRegistry.registerModelLayer(MODEL_RESOURCE_BEE_LAYER, BeeEntityModel::getTexturedModelData);
    }
}
