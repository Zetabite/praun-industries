package zetabite.praunindustries;

// Minecraft / Mojang
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.Identifier;

// Quilt
import org.quiltmc.loader.api.ModContainer;
import org.quiltmc.qsl.base.api.entrypoint.ModInitializer;
import org.quiltmc.qsl.item.group.api.QuiltItemGroup;

// Other
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

// Praun Industries
import org.quiltmc.qsl.lifecycle.api.event.ServerTickEvents;
import zetabite.praunindustries.bees.BeeGenome;
import zetabite.praunindustries.bees.BeeTypeImpl;
import zetabite.praunindustries.block.ModBlocks;
import zetabite.praunindustries.block.entity.ModBlockEntities;
import zetabite.praunindustries.entity.ModEntities;
import zetabite.praunindustries.fluid.ModFluids;
import zetabite.praunindustries.item.ModItems;
import zetabite.praunindustries.recipe.ModRecipes;
import zetabite.praunindustries.worldgen.ModWorldGen;

import static zetabite.praunindustries.machines.systems.CokeOvenManager.COKE_OVEN_MANAGER;

public class PraunIndustries implements ModInitializer {

	public static final String MODID = "praunindustries";
	public static final String MOD_NAME = "Praun's Industry";
	public static final Logger LOGGER = LogManager.getLogger(MOD_NAME);
	public static ItemGroup ITEM_GROUP = QuiltItemGroup.createWithIcon(new Identifier(MODID, "item_group"),
			() -> new ItemStack(ModBlocks.COKE_OVEN_PERIPHERAL));

	private static final BiMap<Identifier, BeeGenome> BASE_GENOMES = HashBiMap.create();

	public static void registerBaseGenome(Identifier id) {
		BASE_GENOMES.put(id, new BeeGenome(id));
	}

	public static boolean hasBaseGenome(Identifier id) {
		return BASE_GENOMES.containsKey(id);
	}

	@Override
	public void onInitialize(ModContainer mod) {
		ServerTickEvents.START.register(server -> {
			for (ServerWorld world : server.getWorlds()) {
				COKE_OVEN_MANAGER.tick(world);
			}
		});

		ModBlocks.onInitialize();
		ModBlockEntities.onInitialize();
		ModItems.onInitialize();
		BeeTypeImpl.onInitialize();
		ModFluids.onInitialize();
		ModWorldGen.onInitialize();
		ModEntities.onInitialize();
		ModRecipes.onInitialize();

		LOGGER.info(MOD_NAME + " commercialized this log section!");
	}
}
