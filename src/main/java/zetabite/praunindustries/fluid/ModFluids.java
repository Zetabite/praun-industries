package zetabite.praunindustries.fluid;

// Minecraft / Mojang
import net.fabricmc.fabric.api.registry.FuelRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.block.FluidBlock;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.fluid.FlowableFluid;
import net.minecraft.fluid.Fluid;
import net.minecraft.item.*;

// Fabric
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.render.fluid.v1.FluidRenderHandlerRegistry;
import net.fabricmc.fabric.api.client.render.fluid.v1.SimpleFluidRenderHandler;

// Quilt
import net.minecraft.tag.TagKey;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import org.quiltmc.qsl.block.extensions.api.client.BlockRenderLayerMap;
import org.quiltmc.qsl.block.extensions.api.QuiltBlockSettings;
import org.quiltmc.qsl.item.setting.api.QuiltItemSettings;

// Praun Industries
import zetabite.praunindustries.util.ModContentID;
import zetabite.praunindustries.PraunIndustries;
import zetabite.praunindustries.util.ContentRegistryUtil;

public class ModFluids extends ContentRegistryUtil {
    public static final FlowableFluid STILL_CREOSOTE;
    public static final FlowableFluid FLOWING_CREOSOTE;
    public static final Item CREOSOTE_BUCKET;
    public static final Item CREOSOTE_BOTTLE;
    public static final Block CREOSOTE;
    public static final TagKey<Fluid> CREOSOTE_FLUIDS;
    public static final TagKey<Item> CREOSOTE_BUCKETS;
    public static final TagKey<Item> CREOSOTE_BOTTLES;

    private static void registerWaterLikeFluid(FlowableFluid stillFluid, FlowableFluid flowingFluid, int tint) {
        FluidRenderHandlerRegistry.INSTANCE.register(stillFluid, flowingFluid, SimpleFluidRenderHandler.coloredWater(tint));
        BlockRenderLayerMap.put(RenderLayer.getTranslucent(), stillFluid, flowingFluid);
    }

    static {
        STILL_CREOSOTE = registerFlowableFluid(ModContentID.CREOSOTE, new CreosoteFluid.Still());
        FLOWING_CREOSOTE = registerFlowableFluid("flowing_" + ModContentID.CREOSOTE, new CreosoteFluid.Flowing());
        CREOSOTE_BUCKET = registerItem(ModContentID.CREOSOTE + "_bucket", new CreosoteBucketItem(ModFluids.STILL_CREOSOTE, new QuiltItemSettings().group(PraunIndustries.ITEM_GROUP).recipeRemainder(Items.BUCKET).maxCount(1)));
        CREOSOTE_BOTTLE = registerItem(ModContentID.CREOSOTE + "_bottle", new CreosoteBottleItem(new QuiltItemSettings().group(PraunIndustries.ITEM_GROUP).recipeRemainder(Items.GLASS_BOTTLE).maxCount(16)));
        CREOSOTE = registerBlock(ModContentID.CREOSOTE, new FluidBlock(STILL_CREOSOTE, QuiltBlockSettings.copy(Blocks.WATER)){});
        CREOSOTE_FLUIDS = TagKey.of(Registry.FLUID_KEY, new Identifier("c", ModContentID.CREOSOTE));
        CREOSOTE_BUCKETS = TagKey.of(Registry.ITEM_KEY, new Identifier("c", ModContentID.CREOSOTE + "_bucket"));
        CREOSOTE_BOTTLES = TagKey.of(Registry.ITEM_KEY, new Identifier("c", ModContentID.CREOSOTE + "_bottle"));
    }

    public static void onInitialize() {
        FuelRegistry.INSTANCE.add(CREOSOTE_BUCKET, 900);
        FuelRegistry.INSTANCE.add(CREOSOTE_BOTTLE, 80);
    }

    @Environment(EnvType.CLIENT)
    public static void onInitializeClient() {
        registerWaterLikeFluid(ModFluids.STILL_CREOSOTE, ModFluids.FLOWING_CREOSOTE, 0xAAAC23);
    }
}
