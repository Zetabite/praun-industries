package zetabite.praunindustries.fluid;

// Minecraft / Mojang
import net.minecraft.block.BlockState;
import net.minecraft.block.FluidBlock;
import net.minecraft.fluid.Fluid;
import net.minecraft.fluid.FluidState;
import net.minecraft.item.Item;
import net.minecraft.state.StateManager;

public abstract class CreosoteFluid extends WaterLikeFluid {
    // TODO: Fix textures for flowing creosote
    // TODO: Fix behavior of creosote, like movement

    @Override
    public Fluid getFlowing() {
        return ModFluids.FLOWING_CREOSOTE;
    }

    @Override
    public Fluid getStill() {
        return ModFluids.STILL_CREOSOTE;
    }

    @Override
    public Item getBucketItem() {
        return ModFluids.CREOSOTE_BUCKET;
    }

    @Override
    protected BlockState toBlockState(FluidState state) {
        return ModFluids.CREOSOTE.getDefaultState().with(FluidBlock.LEVEL, getBlockStateLevel(state));
    }

    public boolean matchesType(Fluid fluid) {
        return fluid == ModFluids.STILL_CREOSOTE || fluid == ModFluids.FLOWING_CREOSOTE;
    }

    @Override
    protected boolean isInfinite() {
        return false;
    }

    public static class Flowing extends CreosoteFluid {
        @Override
        protected void appendProperties(StateManager.Builder<Fluid, FluidState> builder) {
            super.appendProperties(builder);
            builder.add(LEVEL);
        }

        @Override
        public int getLevel(FluidState fluidState) {
            return fluidState.get(LEVEL);
        }

        @Override
        public boolean isStill(FluidState fluidState) {
            return false;
        }
    }

    public static class Still extends CreosoteFluid {
        @Override
        public int getLevel(FluidState fluidState) {
            return 8;
        }

        @Override
        public boolean isStill(FluidState fluidState) {
            return true;
        }
    }
}
