package zetabite.praunindustries.mixin;

// Minecraft / Mojang
import net.minecraft.entity.EntityType;
import net.minecraft.entity.passive.BeeEntity;
import net.minecraft.entity.passive.PassiveEntity;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.server.world.ServerWorld;

// Mixin
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

// Praun Industries
import zetabite.praunindustries.PraunIndustries;
import zetabite.praunindustries.bees.BeeEntityDuckInterface;
import zetabite.praunindustries.bees.BeeGenome;
import zetabite.praunindustries.bees.BeeType;
import zetabite.praunindustries.bees.entity.passive.ResourceBeeEntity;
import zetabite.praunindustries.entity.ModEntities;


@Mixin(BeeEntity.class)
public abstract class BeeEntityMixin implements BeeEntityDuckInterface {

    private BeeGenome beeGenome = new BeeGenome(BeeType.DEFAULT);

    public BeeGenome getBeeGenome() {
        return beeGenome;
    }

    public void setBeeGenome(BeeGenome beeGenome) {
        this.beeGenome = beeGenome;
    }

    // TODO: Correctly mixin into function
    @Inject(method = "writeCustomDataToNbt", at = @At("TAIL"))
    private void praun_industries$injectToWriteBeeGenome(NbtCompound nbt, CallbackInfo ci) {
        nbt = BeeGenome.writeBeeGenomeNbt(nbt, beeGenome);
    }

    // TODO: Correctly mixin into function
    @Inject(method = "readCustomDataFromNbt", at = @At("TAIL"))
    private void praun_industries$injectToReadBeeGenome(NbtCompound nbt, CallbackInfo ci) {
        beeGenome = BeeGenome.readBeeGenomeNbt(nbt);

        if (beeGenome == null) {
            beeGenome = new BeeGenome(BeeType.DEFAULT);
        }
    }

    @Inject(cancellable = true, method = "createChild", at = @At(value = "HEAD", target = "net/minecraft/entity/passive/BeeEntity.createChild(Lnet/minecraft/server/world/ServerWorld;Lnet/minecraft/entity/passive/PassiveEntity;)Z"))
    private void praun_industries$replaceBreedingResult(ServerWorld serverWorld, PassiveEntity passiveEntity, CallbackInfoReturnable<BeeEntity> cir) {
        BeeGenome childGenome;

        if (passiveEntity instanceof BeeEntity) {
            childGenome = new BeeGenome(((BeeEntityDuckInterface) passiveEntity).getBeeGenome(), this.getBeeGenome());
        } else {
            childGenome = new BeeGenome(this.beeGenome.getBeeType(), this.beeGenome.getDistribution());
        }

        // TODO: Remember to remove logging of nbt after done testing
        if (BeeType.getId(childGenome.getBeeType()).equals(BeeType.getId(BeeType.DEFAULT))) {
            BeeEntity childEntity = EntityType.BEE.create(serverWorld);
            ((BeeEntityDuckInterface) childEntity).setBeeGenome(childGenome);
            NbtCompound nbt = new NbtCompound();
            nbt = BeeGenome.writeBeeGenomeNbt(nbt, ((BeeEntityDuckInterface) childEntity).getBeeGenome());
            PraunIndustries.LOGGER.info("Bee childs " + childEntity + " nbt " + nbt);
            cir.setReturnValue(childEntity);
        } else {
            ResourceBeeEntity childEntity = ModEntities.RESOURCE_BEE.create(serverWorld);
            ((BeeEntityDuckInterface) childEntity).setBeeGenome(childGenome);
            NbtCompound nbt = new NbtCompound();
            nbt = BeeGenome.writeBeeGenomeNbt(nbt, ((BeeEntityDuckInterface) childEntity).getBeeGenome());
            PraunIndustries.LOGGER.info("Bee childs " + childEntity + " nbt " + nbt);
            cir.setReturnValue(childEntity);
        }
    }
}
